# Finess API


[GIT REPOSITORY](https://framagit.org/interhop/toobib/finess-api)

# API Specs

[The openapi specs](https://finess.api.interhop.org/apidoc/swagger)

# Project

```
├── README.md
├── Makefile          # make invokations
├── docs              # markdown project documentation
├── mkdocs.yml        # documentation configuration
├── .gitlab-ci.yml    # ci-cd 
├── requirements.txt  # dependencies
├── api-flask
│   ├── requirements.txt        
│   ├── Dockerfile
│   ├── server
│       ├── api           # core tools
│       ├── model         # sqlalchemy classes
│       ├── schema        # pydantic classes
│       └── __init__.py   # to launch server
├── load-data
│   ├── Makefile      # 'make all' to load and install data
│   ├── irene         # git submodule for irene repo
│   ├── finess-create-table.sql
│   ├── finess-constraints.sql
│   ├── finess-load-row-data.sql
│   ├── finess-load-api.sql
│   ├── omop-create-table.sql
│   ├── omop-constraints.sql
│   ├── omop-load-row-data.sql
│   ├── omop-load-finess-vocabularies.sql
│   └── functions.sql
├── LICENSE 
├── docker-compose-development.yml 
└── docker-compose.yml

```
# Build tools

```
(.venv) finess-api$ make
virtualenv:   Install and run virtualenv
local-doc:    Run documentation locally
prod:         Run production server
dev-docker:   Run development server with docker
dev-flask:    Run development server without docker
```
