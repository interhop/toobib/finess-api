# Sources de données

## Extraction du Registre Officiel des établissements 

- Unité de production: DREES
- Page de présentation: http://finess.sante.gouv.fr/
- Distribution: https://www.data.gouv.fr/fr/datasets/finess-extraction-du-fichier-des-etablissements/
- Lien de téléchargement des données : https://www.data.gouv.fr/fr/datasets/r/51a04fc8-50fa-4844-9b92-b51c69be742e
- Format de données : csv
- Encodage : Latin1
- Mise à jour: tous les 2 mois

Nous utilisons le fichier "Extraction Finess des Etablissements".

A noter que ce CSV est en 2 parties. Les lignes de la première partie du fichier débute par 'structureet' et présentent des informations légales sur la structure (adresse, SIRET...). 
Celles de la seconde partie commençant par 'geolocalisation' et présentent des informations de géolocalisation. Les coordonnées spaciales  sont en [Lambert93](https://fr.wikipedia.org/wiki/Projection_conique_conforme_de_Lambert).

Il existe deux notions importantes :
- "Etablissement FINESS": nofiness**et**
- "Entité juridique FINESS": nofiness**ej**

Un "FINESS juridique" peut "chapoter" plusieurs "FINESS établissement". Quand le "FINESS juridique" change (changement de propriétaire par ex.), le "FINESS établissement" ne change pas.

Les scripts de téléchargement de ce fichier et de chargement de la base de données PostGres sont disponibles [ici](https://framagit.org/interhop/toobib/finess-api/-/tree/master/load-data).
Le [Makefile](https://framagit.org/interhop/toobib/finess-api/-/blob/master/load-data/Makefile) permet de lancer le telechargement puis l'installe dans la base de données : ```make all```

## FINESS au format OMOP

- Unité de production: Martine Martins
- Page de présentation: https://framagit.org/terminos/irene/
- Lien de téléchargement des données : https://framagit.org/terminos/irene/-/tree/main/finess/dat/omop/finess.zip
- Format de données : csv
- Encodage : utf8
- Mise à jour: ?

Une des problématiques du registre officiel des établissements est qu'il n'est pas dans un format standard de géomatique ; il est au format [Lambert93](https://fr.wikipedia.org/wiki/Projection_conique_conforme_de_Lambert).

En même temps que nous transformons les données du FINESS au format OMOP nous en profitons pour exprimer les coordonnées spaciales dans un format standard : le [WSG84](https://en.wikipedia.org/wiki/World_Geodetic_System#WGS84). 

La documentation est disponible ici : https://framagit.org/terminos/irene/-/blob/main/finess/doc/README.md

Le détail du format OMOP ici : https://framagit.org/terminos/irene/-/tree/main/finess/doc#omop

## Base officielle des codes postaux 

- Unité de production: La Poste
- Page de présentation: https://datanova.laposte.fr/explore/dataset/laposte_hexasmal/information
- Distribution: https://www.data.gouv.fr/fr/datasets/finess-extraction-du-fichier-des-etablissements/
- Lien de téléchargement des données : https://datanova.laposte.fr/explore/dataset/laposte_hexasmal/download/?format=csv&timezone=Europe/Berlin&lang=fr&use_labels_for_header=true&csv_separator=|
- Format de données : csv
- Encodage : utf8
- Mise à jour: Semestrielle

