# API FHIR 

## End Point

URL de production : [https://finess.api.interhop.org](https://finess.api.interhop.org)

URL de développement : [https://finess.dev.api.interhop.org](https://finess.dev.api.interhop.org)

Un endpoint OpenAPI est disponible à l'adresse suivante : [https://finess.api.interhop.org/apidoc/swagger](https://finess.api.interhop.org/apidoc/swagger)

## Documentation intéresssante

- [Implémentation de Cerner - Millenium](https://fhir.cerner.com/millennium/r4/entities/)
- [Hapi Fhir](https://hapi.fhir.org/) : [Organization](https://hapi.fhir.org/resource?serverId=home_r4&pretty=true&_summary=&resource=Organization) et [Location](https://hapi.fhir.org/resource?serverId=home_r4&pretty=false&_summary=&resource=Location)

## Considération générales

#### [Pagination](https://www.hl7.org/fhir/http.html#paging)

The pagination links are included in the Bundle when a resource returns more items than the Bundle page size. It is important to follow these Link header values instead of constructing your own URLs.

```json
{
    "resourceType": "bundle",
    "type": "searchset",
    "total": 38,
    "link": [
        {
            "relation": "self",
            "url": "https://finess.api.interhop.org/Organization?_text=aphp&address-city=paris&_page_=1"
        },
        {
            "relation": "next",
            "url": "https://finess.api.interhop.org/Organization?_text=aphp&address-city=paris&_page_=2"
        },
        {
            "relation": "first",
            "url": "https://finess.api.interhop.org/Organization?_text=aphp&address-city=paris&_page_=1"
        },
        {
            "relation": "last",
            "url": "https://finess.api.interhop.org/Organization?_text=aphp&address-city=paris&_page_=4"
        }
    ],
}
```

The possible relation values are :


| Name | Description | 
| -------- | -------- | 
| self     | Shows the URL of the current page of results.     |
| next     | Shows the URL of the immediate next page of results.     |
| previous     | If paging, shows the URL of the previous page of results.     |
| first     | An URL that refers to the furthest preceding resource in a series of resources.     |
| last     | An URL that refers to the furthest following resource in a series of resources.     |

## [Extension](https://www.hl7.org/fhir/extensibility.html)

Comme décrit dans le [Dossier des spécifications Fonctionnelles et Techniques](https://esante.gouv.fr/sites/default/files/media_entity/documents/annuaire_interrogation-synchrone-v1_dsft_2020_06-30_v1.0.pdf) il est nécessaire de fournir une extension sur le département dans le [datatype](https://www.hl7.org/fhir/datatypes.html) nommé [Address](https://www.hl7.org/fhir/datatypes.html#address).

| Name | Description | Type | 
| -------- | -------- | ---- |
| url     | Shows the URL of the current page of results.     | [uri](https://www.hl7.org/fhir/datatypes.html#uri) | 
| valueCodeableConcept     |   Value of extension   | [CodeableConcept](https://www.hl7.org/fhir/datatypes.html#codeableconcept) |

## [Organization](https://www.hl7.org/fhir/organization.html)


### Overview

The Organization resource describes a grouping of people or business entities relevant to the healthcare process. Organizations include hospitals, employers, insurance companies, physicians’ offices, rehabilitation facilities, laboratories, etc.

### Retrieve by id

An individual Organization by its id:

```GET /Organization/<uuid:_id>```

#### Parameters

No parameters

#### Examples

##### Requests

```GET https://finess.api.interhop.org/Organization/6fffa757-2839-48f1-a1c4-cb28ba42e036```


##### Response
```json
{
    "id": "6fffa757-2839-48f1-a1c4-cb28ba42e036",
    "resourceType": "Organization",
    "name": "CH DE FLEYRIAT",
    "alias": [],
    "active": [
        false
    ],
    "address": [
        {
            "city": "VIRIAT",
            "country": "",
            "district": "",
            "extension": [
                {
                    "url": "https://apifhir.annuaire.sante.fr/exposed/structuredefinition/french-department",
                    "valueCodeableConcept": {
                        "coding": [
                            {
                                "code": "01",
                                "display": "AIN",
                                "system": "TRE-G09-DepartementOM"
                            }
                        ],
                        "text": "AIN"
                    }
                }
            ],
            "line": [
                "900",
                "RTE DE PARIS"
            ],
            "period": null,
            "postal_code": "01440",
            "state": "",
            "text": "900 RTE DE PARIS VIRIAT",
            "type": "postal",
            "use": "work"
        }
    ],
    "identifier": [
        {
            "period": null,
            "system": "https://toobib.org/organization_identifier/finess_ej",
            "type": {
                "coding": [
                    {
                        "code": "FINESS",
                        "display": "",
                        "system": "https://toobib.org/organization_identifier"
                    }
                ],
                "text": ""
            },
            "use": "official",
            "value": "010780054"
        },
        {
            "period": null,
            "system": "https://toobib.org/organization_identifier/siret",
            "type": {
                "coding": [
                    {
                        "code": "SIRET",
                        "display": "",
                        "system": "https://toobib.org/organization_identifier"
                    }
                ],
                "text": ""
            },
            "use": "official",
            "value": "26010004500012"
        }
    ],
    "telecom": [
        {
            "period": null,
            "rank": 1,
            "system": "phone",
            "use": "work",
            "value": "0474454647"
        },
        {
            "period": null,
            "rank": 1,
            "system": "fax",
            "use": "work",
            "value": "0474454114"
        }
    ],
    "type": [
        {
            "coding": [
                {
                    "code": "355",
                    "display": "Centre Hospitalier (C.H.)",
                    "system": "TRE-R66-CategorieEtablissement"
                }
            ],
            "text": "Centre Hospitalier (C.H.)"
        }
    ]
}
```

### Search

```GET /Organization?:parameters```

#### Parameters



| Name         | Required     | Type          | Description |
| --------     | --------     | --------      | ----|
| \_id         | This or any other required search parameter         | [Token](http://hl7.org/fhir/r4/search.html#token)          |    The logical resource id associated with the resource.     |
| identifier   | This or any other required search parameter         | [Token](http://hl7.org/fhir/r4/search.html#token)          | FINESS or SIRET or SIREN. Example: https://toobib.api.interhop.org/Organization?identifier=finess\|010000024|
| address-city|This, or any other required search parameter|[String](https://hl7.org/fhir/R4/search.html#string)|A city specified in an address|
| french-department   | This is an extension. This or any other required search parameter         | [Token](http://hl7.org/fhir/r4/search.html#token)          | Have to be 'TRE-G09-DepartementOM'. Example: https://toobib.api.interhop.org/Organization?french-department=TRE-G09-DepartementOM\|59|
| name |This, or any other required search parameter|[String](https://hl7.org/fhir/R4/search.html#string)|Name used for the organization.|
| \_text |This, or any other required search parameter|[String](https://hl7.org/fhir/R4/search.html#string)|[Search on the narrative](https://www.hl7.org/fhir/search.html#content) of the resource (given and name)|
| page |No|[Number](https://hl7.org/fhir/r4/search.html#number)|[Pagination](https://www.hl7.org/fhir/http.html#paging).  Defaults to 1.|
| \_count |No|[Number](https://hl7.org/fhir/r4/search.html#number)|The maximum number of results to return. Defaults to 12.|


> When the ```_id``` is provided, the other parameters are ignored

> When provided, the ```identifier``` query parameter must include both a system and a code. For practitioner resource, the system could only be siret, siren or finess

> Search with ```address``` parameters is not possible only ```address-city```

#### Examples

##### Requests
```GET https://finess.api.interhop.org/Organization?_id=6fffa757-2839-48f1-a1c4-cb28ba42e036```

##### Response
```json

{
    "type": "searchset",
    "resourceType": "bundle",
    "total": 1,
    "entry": [

           {
            "fullURL": "https://finess.dev.api.interhop.orgOrganization/6fffa757-2839-48f1-a1c4-cb28ba42e036",
            "resource": {
                "active": [
                    false
                ],
                "address": [
                    {
                        "city": "VIRIAT",
                        "country": "",
                        "district": "",
                        "extension": [
                            {
                                "url": "https://apifhir.annuaire.sante.fr/exposed/structuredefinition/french-department",
                                "valueCodeableConcept": {
                                    "coding": [
                                        {
                                            "code": "01",
                                            "display": "AIN",
                                            "system": "TRE-G09-DepartementOM"
                                        }
                                    ],
                                    "text": "AIN"
                                }
                            }
                        ],
                        "line": [
                            "900",
                            "RTE DE PARIS"
                        ],
                        "period": null,
                        "postal_code": "01440",
                        "state": "",
                        "text": "900 RTE DE PARIS VIRIAT",
                        "type": "postal",
                        "use": "work"
                    }
                ],
                "alias": [],
                "id": "6fffa757-2839-48f1-a1c4-cb28ba42e036",
                "identifier": [
                    {
                        "period": null,
                        "system": "https://toobib.org/organization_identifier/finess_ej",
                        "type": {
                            "coding": [
                                {
                                    "code": "FINESS",
                                    "display": "",
                                    "system": "https://toobib.org/organization_identifier"
                                }
                            ],
                            "text": ""
                        },
                        "use": "official",
                        "value": "010780054"
                    },
                    {
                        "period": null,
                        "system": "https://toobib.org/organization_identifier/siret",
                        "type": {
                            "coding": [
                                {
                                    "code": "SIRET",
                                    "display": "",
                                    "system": "https://toobib.org/organization_identifier"
                                }
                            ],
                            "text": ""
                        },
                        "use": "official",
                        "value": "26010004500012"
                    }
                ],
                "name": "CH DE FLEYRIAT",
                "resourceType": "Organization",
                "telecom": [
                    {
                        "period": null,
                        "rank": 1,
                        "system": "phone",
                        "use": "work",
                        "value": "0474454647"
                    },
                    {
                        "period": null,
                        "rank": 1,
                        "system": "fax",
                        "use": "work",
                        "value": "0474454114"
                    }
                ],
                "type": [
                    {
                        "coding": [
                            {
                                "code": "355",
                                "display": "Centre Hospitalier (C.H.)",
                                "system": "TRE-R66-CategorieEtablissement"
                            }
                        ],
                        "text": "Centre Hospitalier (C.H.)"
                    }
                ]
            }
        }
    ]
}
```

## [Location](http://www.hl7.org/fhir/location.html)


### Overview

The Location resource describes physical places where healthcare services are provided. In Millennium, facilities are the top level of the patient location hierarchy. A facility is also an Organization resource at which patient locations are associated. All facilities are organizations, but not all organizations are facilities. A location can also be an ambulatory patient care area like a clinic or an emergency room. The location hierarchy from highest to lowest is facility, building, nursing unit, room, and bed.

### Retrieve by id

An individual Location by its id:

```GET /Location/<uuid:_id>```

#### Parameters

No parameters

#### Examples

##### Requests

```GET https://finess.api.interhop.org/Location/6fffa757-2839-48f1-a1c4-cb28ba42e036```


##### Response
```json
{
    "address": [
        {
            "city": "VIRIAT",
            "country": "",
            "district": "",
            "extension": [
                {
                    "url": "https://apifhir.annuaire.sante.fr/exposed/structuredefinition/french-department",
                    "valueCodeableConcept": {
                        "coding": [
                            {
                                "code": "01",
                                "display": "AIN",
                                "system": "TRE-G09-DepartementOM"
                            }
                        ],
                        "text": "AIN"
                    }
                }
            ],
            "line": [
                "900",
                "RTE DE PARIS"
            ],
            "period": null,
            "postal_code": "01440",
            "state": "",
            "text": "900 RTE DE PARIS VIRIAT",
            "type": "postal",
            "use": "work"
        }
    ],
    "alias": [],
    "id": "6fffa757-2839-48f1-a1c4-cb28ba42e036",
    "identifier": [
        {
            "period": null,
            "system": "https://toobib.org/location_identifier/finess_et",
            "type": {
                "coding": [
                    {
                        "code": "FINESS",
                        "display": "",
                        "system": "https://toobib.org/location_identifier"
                    }
                ],
                "text": ""
            },
            "use": "official",
            "value": "010000024"
        }
    ],
    "name": "CH DE FLEYRIAT",
    "position": {
        "latitude": 46.2227448320244,
        "longitude": 5.20859613264196
    },
    "resourceType": "Location",
    "status": [
        "active"
    ],
    "telecom": [
        {
            "period": null,
            "rank": 1,
            "system": "phone",
            "use": "work",
            "value": "0474454647"
        },
        {
            "period": null,
            "rank": 1,
            "system": "fax",
            "use": "work",
            "value": "0474454114"
        }
    ],
    "type": [
        {
            "coding": [
                {
                    "code": "355",
                    "display": "Centre Hospitalier (C.H.)",
                    "system": "TRE-R66-CategorieEtablissement"
                }
            ],
            "text": "Centre Hospitalier (C.H.)"
        }
    ]
}
```

### Search

```GET /Location?:parameters```

#### Parameters



| Name         | Required     | Type          | Description |
| --------     | --------     | --------      | ----|
| \_id         | This or any other required search parameter         | [Token](http://hl7.org/fhir/r4/search.html#token)          |    The logical resource id associated with the resource.     |
| identifier   | This or any other required search parameter         | [Token](http://hl7.org/fhir/r4/search.html#token)          | FINESS or SIRET or SIREN. Example: https://toobib.api.interhop.org/Organization?identifier=finess\|010000024|
| address-city|This, or any other required search parameter|[String](https://hl7.org/fhir/R4/search.html#string)|A city specified in an address|
| french-department   | This is an extension. This or any other required search parameter         | [Token](http://hl7.org/fhir/r4/search.html#token)          | Have to be 'TRE-G09-DepartementOM'. Example: https://toobib.api.interhop.org/Organization?french-department=TRE-G09-DepartementOM\|59|
| name |This, or any other required search parameter|[String](https://hl7.org/fhir/R4/search.html#string)|Name used for the organization.|
| \_text |This, or any other required search parameter|[String](https://hl7.org/fhir/R4/search.html#string)|[Search on the narrative](https://www.hl7.org/fhir/search.html#content) of the resource (given and name)|
| page |No|[Number](https://hl7.org/fhir/r4/search.html#number)|[Pagination](https://www.hl7.org/fhir/http.html#paging).  Defaults to 1.|
| \_count |No|[Number](https://hl7.org/fhir/r4/search.html#number)|The maximum number of results to return. Defaults to 12.|


> When the ```_id``` is provided, the other parameters are ignored

> When provided, the ```identifier``` query parameter must include both a system and a code. For practitioner resource, the system could only be siret, siren or finess

> Search with ```address``` parameters is not possible only ```address-city```

#### Examples

##### Requests
```GET https://finess.api.interhop.org/Location?_id=6fffa757-2839-48f1-a1c4-cb28ba42e036```

##### Response
```json

{
    "type": "searchset",
    "resourceType": "bundle",
    "total": 1,
    "entry": [
        {
            "fullURL": "https://finess.dev.api.interhop.orgLocation/6fffa757-2839-48f1-a1c4-cb28ba42e036",
            "resource": {
                "address": [
                    {
                        "city": "VIRIAT",
                        "country": "",
                        "district": "",
                        "extension": [
                            {
                                "url": "https://apifhir.annuaire.sante.fr/exposed/structuredefinition/french-department",
                                "valueCodeableConcept": {
                                    "coding": [
                                        {
                                            "code": "01",
                                            "display": "AIN",
                                            "system": "TRE-G09-DepartementOM"
                                        }
                                    ],
                                    "text": "AIN"
                                }
                            }
                        ],
                        "line": [
                            "900",
                            "RTE DE PARIS"
                        ],
                        "period": null,
                        "postal_code": "01440",
                        "state": "",
                        "text": "900 RTE DE PARIS VIRIAT",
                        "type": "postal",
                        "use": "work"
                    }
                ],
                "alias": [],
                "id": "6fffa757-2839-48f1-a1c4-cb28ba42e036",
                "identifier": [
                    {
                        "period": null,
                        "system": "https://toobib.org/location_identifier/finess_et",
                        "type": {
                            "coding": [
                                {
                                    "code": "FINESS",
                                    "display": "",
                                    "system": "https://toobib.org/location_identifier"
                                }
                            ],
                            "text": ""
                        },
                        "use": "official",
                        "value": "010000024"
                    }
                ],
                "name": "CH DE FLEYRIAT",
                "position": {
                    "latitude": 46.2227448320244,
                    "longitude": 5.20859613264196
                },
                "resourceType": "Location",
                "status": [
                    "active"
                ],
                "telecom": [
                    {
                        "period": null,
                        "rank": 1,
                        "system": "phone",
                        "use": "work",
                        "value": "0474454647"
                    },
                    {
                        "period": null,
                        "rank": 1,
                        "system": "fax",
                        "use": "work",
                        "value": "0474454114"
                    }
                ],
                "type": [
                    {
                        "coding": [
                            {
                                "code": "355",
                                "display": "Centre Hospitalier (C.H.)",
                                "system": "TRE-R66-CategorieEtablissement"
                            }
                        ],
                        "text": "Centre Hospitalier (C.H.)"
                    }
                ]
            }
        }
    ],
    "link": [
        {
            "relation": "self",
            "url": "https://finess.dev.api.interhop.org/Location?_id=6fffa757-2839-48f1-a1c4-cb28ba42e036&page=1"
        },
        {
            "relation": "first",
            "url": "https://finess.dev.api.interhop.org/Location?_id=6fffa757-2839-48f1-a1c4-cb28ba42e036&page=1"
        },
        {
            "relation": "last",
            "url": "https://finess.dev.api.interhop.org/Location?_id=6fffa757-2839-48f1-a1c4-cb28ba42e036&page=1"
        }
    ],
}
```
