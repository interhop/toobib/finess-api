# Helper

## Useful docker commands

```bash
sudo docker-compose build
```

If you want to run your services in the background, you can pass the -d flag (for “detached” mode)
```bash
sudo docker-compose up -d
```

Following the log of a specific container
```bash
sudo docker-compose logs -f server
```

Getting a shell in a container
```bash
sudo docker-compose exec -i -t server /bin/bash
```

Stop the reunning services
```bash
docker-compose stop
```

Removing the containers entirely and cleaning up
```bash
sudo docker-compose down --volumes
```

To see what is currently running services:
```bash
sudo docker-compose ps
```
