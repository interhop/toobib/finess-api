# Explication de la base de données

## Schema ```finess```

Voici le [ddl](https://framagit.org/interhop/toobib/finess-api/-/blob/master/load-data/finess-create-table.sql) de l'ensemble de ces tables.

### A partir du ```registre officiel des établissements```

#### Table ```finess_struct```

Contient toutes les lignes de la première partie du csv ```registre officiel des établissements``` commençant par 'structureet'. Ce fichier contient l'adresse, les codes uniques (siret, finess), les coordonnées téléphoniques....


#### Table ```finess_geo```

Contient toutes les lignes de la seconde partie du csv ```registre officiel des établissements``` commençant par 'geolocalisation'. Ce fichier contient les coordonnées géographiques en [Lambert93](https://fr.wikipedia.org/wiki/Projection_conique_conforme_de_Lambert).

### A partir de la table ```laposte```

Ces tables permettent de dédoublonner sur le code_postal ou le code_commune_insee

#### Table ```laposte_postal```

```sql
INSERT INTO laposte_postal (
	code_postal
	, gps
)
WITH tmp AS (
	SELECT *,
	row_number() over (partition by code_postal order by code_postal asc) as rn
	from laposte
)
SELECT code_postal, gps
FROM tmp
where rn = 1
;
```


#### Table ```laposte_commune```


```sql
INSERT INTO laposte_commune (
	code_commune_insee
	, gps

)
WITH tmp AS (
	SELECT *,
	row_number() over (partition by code_commune_insee order by code_commune_insee asc) as rn
	from laposte
)
SELECT code_commune_insee, gps
FROM tmp
where rn = 1
;

```

### A  partir ```finess.finess_struct```, ```finess.finess_geo```, ```finess.laposte_commune```, ```finess.laposte_postal``` et ```omop.care_site```

#### Table ```care_site```

Contient les informations des structures (FINESS), des coordonnées géographiques en Lambert93, en WSG82 (issues des tables OMOP), et des coordonnées géographiques issues des fichiers officiels maintenus par la poste.

### A partir de ```care_site```

#### Vue materialisée ```care_site_search```

Cette table permet de réaliser une recherche textuelle. Nous avons pris le parti d'utiliser Postgres y compris pour le moteur textuel. 
Nous n'excluons pas l'utilisons de Lucene dans un futur proche en fonction des besoins.

```sql_more=
CREATE MATERIALIZED VIEW care_site_search AS

SELECT care.care_site_id                                                                                     	AS care_site_id
, care.care_site_uuid 												AS care_site_uuid
,  care.finess_et                                                                                          	AS finess_et
,  care.finess_ej                                                                                         	AS finess_ej
,  care.siret                                                                                          		AS siret
,  care.raison_sociale                                                                                          AS nom
,  coalesce(finess.multi_replace(lower(public.unaccent(care.raison_sociale)), '{" ": "", "-":""}'::jsonb), '')  	AS nom_normalise
,  c_dep.concept_code                                                                                         	AS departement
,  c_cat_et.concept_code                                                                                         	AS categorie_etablissement
,  care.code_postal                                                                                         		AS code_postal
,  care.commune                                                                                         		AS commune
,  coalesce(finess.multi_replace(lower(public.unaccent(care.commune)), '{" ": "", "-":""}'::jsonb), '')         	AS commune_normalise

FROM finess.care_site  care
LEFT JOIN omop.concept c_dep ON (care.departement_concept_id = c_dep.concept_id)
    LEFT JOIN omop.concept c_cat_et ON (care.categorie_etablissement_concept_id = c_cat_et.concept_id)

;

```

## Schema```omop```

Il est possible de trouver plus d'information à propos du modèle OMOP sur le [ddl](https://github.com/OHDSI/CommonDataModel/blob/master/PostgreSQL/) ainsi que sur la dernière version de ce modèle [OMOP v6.0](https://ohdsi.github.io/CommonDataModel/cdm60.html),


Nous avons chargé les tables ```concept```, ```concept_relationshop```, ```concept_class```,```concept_synonym```, ```domain```, ```vocabulary```, ```caresite```, ```location``` et ```provider```. Toutes ces tables proviennent directement du dépôt git https://framagit.org/terminos/irene/


En plus des concepts issus de ce dépôts, viennent compléter les terminologies issues de la [nomenclature officielle des objets de santé](https://esante.gouv.fr/interoperabilite/mos-nos/nos).
Les terminologies suivantes sont utilisées par la classification du FINESS : ```TRE-G09-DepartementOM```, ```TRE-R65-AgregatCategorieEtablissement```, ```TRE-R66-CategorieEtablissement```, ```TRE-R73-ESPIC-FHIR``` et ```TRE-R74-ModeFixationTarifaire```. Nous les avons donc chargées dans les tables de vocabulaire OMOP.

