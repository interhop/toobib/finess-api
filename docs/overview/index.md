# Vue d'ensemble

## FINESS : c'est quoi ?

Le Fichier National des Etablissements Sanitaires et Sociaux, nommé usuellement FINESS ou répertoire FINESS, a été mis au point et est géré par le Ministère des Affaires sociales, de la Santé.

Ce fichier constitue, au plan national, la référence en matière d’inventaire des structures et équipements des domaines sanitaire, médico-social, social et de formation aux professions de ces secteurs

Le FINESS est donc essentiel, en France, pour identifier un établissement de santé.

## Notre but

Nous voulons fournir un webservice standard pour pouvoir librement consommer le FINESS. Notre API REST standard utilise le format [FHIR](http://hl7.org/fhir/). 

Cette API REST utilise des technologies (backend) à l'état de l'art. Elle s'appuie sur un framework léger et très utilisé dans la communauté Python : [Flask](http://flask.pocoo.org).
Nous utilisons également [Redis](http://redis.io) et [Docker](http://docker.com) pour automatiser les déploiements.

Le code est maintenu par l'association InterHop. Selon ses principes associatifs il est librement utilisable et modifiable. Nous encourageons son utilisation de façon décentralisée.
