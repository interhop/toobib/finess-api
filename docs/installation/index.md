# Installation et technologies utilisées

## Postgres

PostgreSQL est un gestionnaire de base de données relationnel opensource.

Nous avons choisi d'installer Postgres en local sur la machine (pas de déploiement  dans un container Docker).

### Installation de la base de données

- Prérequis : installation de PostgresSQL.

- Connexion à postgres
```bash
sudo -u postgres psql
```

- Création d'un user and de la database. Dans notre cas le user est finess et la db est finess.
```psql
CREATE USER USER WITH PASSWORD 'PWD' CREATEDB;
CREATE DATABASE "DB" OWNER "USER";
```

- Installation des extensions pg_trgm, uuid-ossp et unaccent:
```psql
\c "DB"
CREATE EXTENSION IF NOT EXISTS unaccent  with schema public;
CREATE EXTENSION IF NOT EXISTS "uuid-ossp"  with schema public;
CREATE EXTENSION IF NOT EXISTS pg_trgm with schema public;
\df -- to check if functions are installed.
```

- Exit psql
```psql
\q
```

### Installation des données

- cd load-data 

- Editer les macros  SCHEMA, USER, DB in Makefile si besoin.

- Créer un fichier [config](https://wiki.postgresql.org/wiki/Pgpass) ```.pgpass``` dans le home de l'utilisateur. Il doit être de la forme

```bash
"IP":"PORT":"DB":"USER":"PWD"
```

Exemple: 
```bash
localhost:5432:finess:finess:password
```
- Appliquer les droits nécessaires au fichier ```.pgpass```
```bash
sudo chmod 600 ~/.pgpass
```

- Run Makefile
```bash
make all
```

### Configuration de postgres pour la production

Cette étape n'est pas nécessaire en version de développement.

En version de développment Redis et l'application flask sont installées sur le network local : localhost. Ceci n'est pas une bonne pratique pour la production mais permet de faciliter de déploiement de la version de développement


#### Connaitre les spécifications de sa machine
- ```cat /proc/version``` : système d'exploitation
- ```cat /proc/meminfo``` : RAM
- ```cat /proc/cpuinfo``` : nombre de CPU
- ```cat /sys/block/sda/queue/rotational``` : 0, il s’agit d’un disque SSD
• 1 ; il s’agit d’un disque SATA ou SCSI

#### configurer : ```postgres.conf```

Rentrer les informations sus-recueillis (OS, RAM, CPU, SSD ...) pour autoconfiguration : https://pgtune.leopard.in.ua/#/

Rajouter ces lignes en fin de fichier ```postgres.conf```

#### configurer : ```pg_hba.conf```

Rajouter l'IPv4 du docker dans le fichier ```pg_hba.conf``` : autorise postgres à échanger avec nos containers dockers regroupés dans un réseau accessible à l'IP 92.168.201.0/24 . 

Rajouter donc la ligne :
```
# IPv4 local connections:
host    all             all             192.168.201.0/24        md5 #Finess Network = IP_INTERNAL_NETWORK (cf. infra)
```

Remarque : cette IPv4 est aussi renseignée dans le fichier ```docker-compose.yml```


#### Créer un fichier ```.env``` à la racine du projet

Ce fichier ```.env``` est chargé au lancement de la commande ```docker-compose up -d ```

Voici un exemple : 
```
UID=finess_user_uid # to fill
APP_FLASK_PORT=5000

INTERNAL_NETWORK_IP=192.168.201.0/24

POSTGRES_DB=finess
POSTGRES_USER=finess
POSTGRES_HOST=ip_de_ma_machine # to fill
POSTGRES_PWD=mon_pwd # to fill
POSTGRES_PORT=5432
```

Créer un utilisateur finess
```bash
adduser --no-create-home finess
```

Récupérer son uid : variable UID du fichier ```.env```
```bash
cat /etc/passwd
```

Attribuer la propriété du dossier ```app-flask``` à l'utilisateur finess
```bash
sudo chown finess:finess -R app-flask
```

Connaitre l'ip de sa machine : variable POSTGRES_HOST du fichier ```.env```
```bash
ip a
```

## Flask

Flask est un micro framework open-source de développement web en Python. Il est classé comme microframework car il est très léger. Flask a pour objectif de garder un noyau simple mais extensible.



### Generate secret key : app-flask/server/config.py


- Créer un fichier ```app-flask/server/config.py```
```bash
vim app-flask/server/config.py
```
- Copier le code suivant dans le fichier ```config.py```. Ne pas oublier de modifier le mot de passe.
``` python
from flask_swagger_ui import get_swaggerui_blueprint
import os

class Config(object):
    SECRET_KEY = secret_key # paste here the value of secret_key

    SQLALCHEMY_TRACK_MODIFICATIONS = False
    PROPAGATE_EXCEPTIONS = True

    USER = 'finess' 
    PWD = 'password' # paste here the same value of PWD declared in the Makefile
    DB = 'finess'
    FINESS_SCHEMA = 'finess' 
    OMOP_SCHEMA = 'omop' 
    
    POSTGRES = {
        'db': os.environ.get('POSTGRES_DB', DB),
        'user': os.environ.get('POSTGRES_USER', USER),
        'pwd': os.environ.get('POSTGRES_PWD', PWD),
        'host': os.environ.get('POSTGRES_HOST', 'localhost'),
        'port': os.environ.get('POSTGRES_PORT', '5432'),
    }
    SQLALCHEMY_DATABASE_URI = 'postgresql://%(user)s:%(pwd)s@%(host)s:%(port)s/%(db)s' % POSTGRES
    PROPAGATE_EXCEPTIONS = True
    
    # JWT CONF
    JWT_SECRET_KEY = jwt_secret_key # paste here the value of jwt_secret_key
    JWT_TOKEN_LOCATION = ['headers', 'query_string']    
    
    SESSION_COOKIE_SECURE = True

class ProductionConfig(Config):
    pass

class DevConfig(Config):
    DEBUG = True
    SESSION_COOKIE_SECURE = False

class TestingConfig(Config):
    TESTING = True
    SESSION_COOKIE_SECURE = False
```

- Ne pas oublier de modifier le secret_key et jwt_secret_key. Voici un exemple pour les générer

``` python
# To generate a new secret key:
# >>> import random, string
# >>> secret_key = "".join([random.choice(string.printable) for _ in range(24)])
# >>> jwt_secret_key = "".join([random.choice(string.printable) for _ in range(24)])
# >>> secret_key + '  ' + jwt_secret_key
```

## Redis

Redis est un système de gestion de base de données fonctionnant sur le principe clé-valeur scalable. Ce système de type NoSQL est connu pour obtenir de très bonnes performances.

Redis est utilisé par améliorer les performances et permet de faire du cache sur les requêtes de notre API-REST.

Avec docker Redis est configurer automatiquement. En cas d'impossibilité d'installer docker ou docker-compose il est possible de mettre un cache Simple in memory sur python. Cf. mode de production

## Mode de production ou de développement : Docker

Pour lancer le serveur Flask et Redis. 

Prérequis : installation de [Docker Engine](https://docs.docker.com/get-docker/) et de [Docker Compose](https://docs.docker.com/compose/install/)

### En production

```bash
sudo docker-compose build
sudo docker-compose up -d
```

Dirigez votre navigateur vers http://localhost:5000.

#### Conteneur ```server```

Le BUILD ; l'image du serveur flask est construite à partir de `app-flask/Dockerfile`. Ce Dockerfile inclut tous les composants python nécessaires et leurs dépendances à partir de `app-flask/requirements.txt`.

Sur le conteneur *web* `command : gunicorn --workers 4 --bind 0.0.0.0:5000 --worker-class="egg:meinheld#gunicorn_worker" --timeout 120 'server:create_app()'` indique à docker-compose de lancer (dans le conteneur serveur) le serveur web gunicorn. *-b :5000* indique à gunicorn de se lier au (écouter le) port 5000 sur n'importe quelle IP du conteneur. *'server:create_app()'* indique à gunicorn l'endroit où se trouve la fabrique d'applications flask. Il indique en fait à gunicorn de rechercher le module python *server* dans son répertoire de travail (que nous avons défini à */app* dans le Dockerfile) et de rechercher la fonction de création d'application *create_app* qui doit créer une instance d'application flask valide. Gunicorn avec ses dépendances et tous les fichiers qui composent notre application web ont été copiés dans l'image pendant le processus de construction du Dockerfile.

Nous avons choisi gunicorn plutôt que uwsgi car le premier semble être plus léger et plus rapide. Par ailleurs la communauté de développeur de gunicorn semble être plus large.

#### Conteneur ```redis```

L'image officielle [redis](https://hub.docker.com/_/redis) prête à l'emploi est utilisée.

#### Mise en réseau

Pour la version de production nous créons un réseau (avec nos deux containers) à l'IP suivante = IP_INTERNAL_NETWORK (192.168.201.0/24 par défaut).

Pour rappel cette IP est celle rajoutée dans le fichier ```pg_hba.conf``` de postgres. Elle autorise donc Postgres à écouter l'IP de nos containers.


### En développement

#### Avec Docker

``` bash
sudo docker-compose build
docker-compose -f docker-compose-development.yml up -d
```
Pour la version de développement nous créons un réseau *'network=host'* le conteneur utilisera le réseau de l'hôte (localhost).
Ceci permet de s'affranchir de l'étape 'Configuration de Postgres pour la Production'.

#### Avec Flask run

Cette configuration permet de ne pas installer de serveur redis et de ne pas avoir à utiliser docker / docker-compose. Par contre il faut opérer un changement dans le fichier `app-flask/server/__init__.py`. Il faut commenter la ligne 21 et décommenter la ligner 22. Le cache n'utilisera pas Redis mais simple la mémoire vive utilisée par Python.

``` bash
  - virtualenv venv
  - source venv/bin/activate
  - pip install -r app-flask/requirements.txt
  - FLASK_APP=app-flask/server FLASK_RUN_PORT=5000 FLASK_ENV=development flask run
```

#### Rechargement automatique en cas de modification du fichier source
Le serveur web de développement flask intégré à la capacité de se recharger automatiquement lorsqu'il détecte un changement dans un fichier source. Cela s'avère pratique pour les étapes de développement.
