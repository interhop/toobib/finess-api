virtualenv:  ## Install dependencies
	virtualenv venv
	source venv/bin/activate
local-doc:  ## Build the docs
	pip install -r docs/requirements.txt
	mkdocs build
	mkdocs serve
prod: ## launch prod mod (cf online doc)
	sudo docker-compose build
	sudo docker-compose up -d
dev-docker: ## launch dev mod (cf online doc)
	sudo docker-compose build
	docker-compose -f docker-compose-development.yml up -d
dev-flask: ## launch dev mod with flask run
	pip install -r app-flask/requirements.txt
	FLASK_APP=app-flask/server FLASK_RUN_PORT=5000 FLASK_ENV=development flask run
