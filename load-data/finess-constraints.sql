/*********************************************************************************
#
#
#     Licence
#
#
#
 ********************************************************************************/

/************************

####### #######  ####### ######     #    ######
   #    #     #  #     # #     #    #    #     #
   #    #     #  #     # #     #    #    #     #
   #    #     #  #     # ######     #    ######
   #    #     #  #     # #     #    #    #     #
   #    #     #  #     # #     #    #    #     #
   #    #######  ####### ######     #    ######

last revised: 08-April-2021

Authors:  Adrien PARROT
Authors:  Quentin PARROT

 *************************/

-------------
-- care_site
-------------
ALTER TABLE care_site ADD CONSTRAINT xpk_care_site_table PRIMARY KEY (care_site_id);
CREATE SEQUENCE care_site_id_seq OWNED BY care_site.care_site_id;
ALTER TABLE care_site ALTER COLUMN care_site_id SET DEFAULT nextval('care_site_id_seq');

-- uniqueness : index vs. constraint
-- https://www.postgresqltutorial.com/postgresql-unique-constraint/
-- https://www.postgresqltutorial.com/postgresql-indexes/postgresql-unique-index/
-- https://medium.com/flatiron-engineering/uniqueness-in-postgresql-constraints-versus-indexes-4cf957a472fd

ALTER TABLE care_site ALTER COLUMN care_site_uuid SET DEFAULT public.uuid_generate_v4 ();
ALTER TABLE care_site ALTER COLUMN insert_datetime SET DEFAULT now();
