/*********************************************************************************
#
#
#     Licence
#
#
#
 ********************************************************************************/

/************************

####### #######  ####### ######     #    ######
   #    #     #  #     # #     #    #    #     #
   #    #     #  #     # #     #    #    #     #
   #    #     #  #     # ######     #    ######
   #    #     #  #     # #     #    #    #     #
   #    #     #  #     # #     #    #    #     #
   #    #######  ####### ######     #    ######

last revised: 14-April-2021

Authors:  Adrien PARROT
Authors:  Quentin PARROT

 *************************/


--- care_site_search

DROP MATERIALIZED VIEW IF EXISTS care_site_search;

CREATE MATERIALIZED VIEW care_site_search AS

	SELECT care.care_site_id                                                                                     	AS care_site_id
	, care.care_site_uuid 												AS care_site_uuid
	,  care.finess_et                                                                                          	AS finess_et
	,  care.finess_ej                                                                                         	AS finess_ej
	,  care.siret                                                                                          		AS siret
	,  care.raison_sociale                                                                                          AS nom
	, coalesce(finess.multi_replace(lower(public.unaccent(care.raison_sociale)), '{" ": "", "-":""}'::jsonb), '')  	AS nom_normalise
	,  c_dep.concept_code                                                                                         	AS departement
	,  c_cat_et.concept_code                                                                                         	AS categorie_etablissement
	,  care.code_postal                                                                                         		AS code_postal
	,  care.commune                                                                                         		AS commune
	, coalesce(finess.multi_replace(lower(public.unaccent(care.commune)), '{" ": "", "-":""}'::jsonb), '')         	AS commune_normalise

	FROM finess.care_site  care
	LEFT JOIN omop.concept c_dep ON (care.departement_concept_id = c_dep.concept_id)
		LEFT JOIN omop.concept c_cat_et ON (care.categorie_etablissement_concept_id = c_cat_et.concept_id)

;

/************************

Create Index

*************************/

--- official_care_site_search
CREATE UNIQUE INDEX CONCURRENTLY idx_official_care_site_search_uuid 			ON care_site_search  (care_site_uuid ASC);
CREATE INDEX CONCURRENTLY idx_official_care_site_search_finess_et 			ON care_site_search 	(finess_et ASC);	
CREATE INDEX CONCURRENTLY idx_official_care_site_search_finess_ej 			ON care_site_search 	(finess_ej ASC);	
CREATE INDEX CONCURRENTLY idx_official_care_site_search_siret 				ON care_site_search 	(siret ASC);
CREATE INDEX CONCURRENTLY idx_official_care_site_search_departement 			ON care_site_search 	(departement ASC);
CREATE INDEX CONCURRENTLY idx_official_care_site_search_categorie_etablissement 	ON care_site_search 	(categorie_etablissement ASC);
CREATE INDEX CONCURRENTLY idx_official_care_site_search_code_postal 			ON care_site_search 	(code_postal ASC);
CREATE INDEX CONCURRENTLY idx_official_care_site_search_nom_normalise 			ON care_site_search 		USING GIN (nom_normalise public.gin_trgm_ops);
CREATE INDEX CONCURRENTLY idx_official_care_site_search_commune_normalise 		ON care_site_search 		USING GIN (commune_normalise public.gin_trgm_ops);
