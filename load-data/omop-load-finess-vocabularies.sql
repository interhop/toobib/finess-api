/*********************************************************************************
#
#
#     Licence
#
#
#
 ********************************************************************************/

/************************

####### #######  ####### ######     #    ######
   #    #     #  #     # #     #    #    #     #
   #    #     #  #     # #     #    #    #     #
   #    #     #  #     # ######     #    ######
   #    #     #  #     # #     #    #    #     #
   #    #     #  #     # #     #    #    #     #
   #    #######  ####### ######     #    ######

last revised: 04-Feb-2021

Authors:  Adrien PARROT
Authors:  Quentin PARROT

 *************************/

-- categorie_etablissement
INSERT INTO concept (
	concept_name
	, concept_code                                 
	, domain_id
	, vocabulary_id
)
SELECT DISTINCT p.libelle_categorie_etablissement
	, p.categorie_etablissement
	, 'CARE_SITE'
	, 'TRE-R66-CategorieEtablissement'
FROM finess.finess_struct p
WHERE p.categorie_etablissement IS NOT NULL
;

-- categorie_agregat_etablissement
INSERT INTO concept (
	concept_name
	, concept_code                                 
	, domain_id
	, vocabulary_id
)
SELECT DISTINCT p.libelle_categorie_agregat_etablissement
	, p.categorie_agregat_etablissement
	, 'CARE_SITE'
	, 'TRE-R65-AgregatCategorieEtablissement'
FROM finess.finess_struct p
WHERE p.categorie_agregat_etablissement IS NOT NULL
;


-- mft
INSERT INTO concept (
	concept_name
	, concept_code                                 
	, domain_id
	, vocabulary_id
)
SELECT DISTINCT p.libelle_mft
	, p.code_mft
	, 'CARE_SITE'
	, 'TRE-R74-ModeFixationTarifaire'
FROM finess.finess_struct p
WHERE p.code_mft IS NOT NULL
;


-- sph
INSERT INTO concept (
	concept_name
	, concept_code                                 
	, domain_id
	, vocabulary_id
)
SELECT DISTINCT p.libelle_sph
	, p.code_sph
	, 'CARE_SITE'
	, 'TRE-R73-ESPIC-FHIR'
FROM finess.finess_struct p
WHERE p.code_sph IS NOT NULL
;

-- departement
INSERT INTO concept (
	concept_name
	, concept_code                                 
	, domain_id
	, vocabulary_id
)
SELECT DISTINCT p.libelle_departement
	, p.departement
	, 'CARE_SITE'
	, 'TRE-G09-DepartementOM'
FROM finess.finess_struct p
WHERE p.departement IS NOT NULL
;

