/*********************************************************************************
#
#
#     Licence
#
#
#
 ********************************************************************************/

/************************

####### #######  ####### ######     #    ######
   #    #     #  #     # #     #    #    #     #
   #    #     #  #     # #     #    #    #     #
   #    #     #  #     # ######     #    ######
   #    #     #  #     # #     #    #    #     #
   #    #     #  #     # #     #    #    #     #
   #    #######  ####### ######     #    ######

last revised: 04-Feb-2021

Authors:  Adrien PARROT
Authors:  Quentin PARROT

 *************************/


TRUNCATE TABLE concept CASCADE;
TRUNCATE TABLE concept_class CASCADE;
TRUNCATE TABLE concept_relationship CASCADE;
TRUNCATE TABLE concept_synonym CASCADE;
TRUNCATE TABLE domain CASCADE;
TRUNCATE TABLE relationship CASCADE;
TRUNCATE TABLE vocabulary CASCADE;
TRUNCATE TABLE care_site CASCADE;
TRUNCATE TABLE location CASCADE;
TRUNCATE TABLE provider CASCADE;
TRUNCATE TABLE fact_relationship CASCADE;

-- DROP INDEX IF EXISTS idx_laposte_code_commune;
-- DROP INDEX IF EXISTS idx_laposte_code_postal;

\copy domain FROM 'download/finess/DOMAIN.csv' WITH DELIMITER E'\t' CSV HEADER ;
\copy concept FROM 'download/finess/CONCEPT.csv' WITH DELIMITER E'\t' CSV HEADER ;
\copy concept_class FROM 'download/finess/CONCEPT_CLASS.csv' WITH DELIMITER E'\t' CSV HEADER ;
\copy concept_synonym FROM 'download/finess/CONCEPT_SYNONYM.csv' WITH DELIMITER E'\t' CSV HEADER ;
\copy relationship FROM 'download/finess/RELATIONSHIP.csv' WITH DELIMITER E'\t' CSV HEADER ;
\copy vocabulary FROM 'download/finess/VOCABULARY.csv' WITH DELIMITER E'\t' CSV HEADER ;
\copy concept_relationship FROM 'download/finess/CONCEPT_RELATIONSHIP.csv' WITH DELIMITER E'\t' CSV HEADER ;
\copy provider FROM 'download/finess/PROVIDER.csv' WITH DELIMITER E'\t' CSV HEADER ;
\copy location FROM 'download/finess/LOCATION.csv' WITH DELIMITER E'\t' CSV HEADER ;
\copy care_site FROM 'download/finess/CARE_SITE.csv' WITH DELIMITER E'\t' CSV HEADER ;
\copy fact_relationship FROM 'download/finess/FACT_RELATIONSHIP.csv' WITH DELIMITER E'\t' CSV HEADER ;




/************************

Create Index

************************/


-- CREATE INDEX idx_finess_struct_finess_et  ON  finess_struct (finess_et ASC);
-- CLUSTER finess_struct USING idx_finess_struct_finess_et;
