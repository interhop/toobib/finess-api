/*********************************************************************************
#
#
#     Licence
#
#
#
 ********************************************************************************/

/************************

####### #######  ####### ######     #    ###### 
   #    #     #  #     # #     #    #    #     #
   #    #     #  #     # #     #    #    #     #
   #    #     #  #     # ######     #    ###### 
   #    #     #  #     # #     #    #    #     #
   #    #     #  #     # #     #    #    #     #
   #    #######  ####### ######     #    ###### 

last revised: 08-April-2021

Authors:  Adrien PARROT
Authors:  Quentin PARROT

 *************************/


/************************

FINESS Extraction du Fichier des établissements 

Liste des établissements du domaine sanitaire et social.
Informations sur la Géo-localisation : Le système d’information source contenant les coordonnées géographiques permettant de géo-localiser les établissements

https://www.data.gouv.fr/fr/datasets/finess-extraction-du-fichier-des-etablissements/#_

 ************************/

DROP TABLE IF EXISTS finess_struct;

CREATE TABLE finess_struct (
	finess_et					TEXT		NULL,
	finess_ej                                       TEXT		NULL,
	raison_sociale                                  TEXT		NULL,
	raison_sociale_longue                           TEXT		NULL,
	complement_raison_sociale                       TEXT		NULL,
	complement_distribution                         TEXT		NULL,
	numero_voie					TEXT		NULL,
	type_voie                                       TEXT		NULL,
	libelle_voie                                    TEXT		NULL,
	complement_voie                                 TEXT		NULL,
	lieu_dit_bp                                     TEXT		NULL,
	code_commune                                     TEXT		NULL,
	departement					TEXT		NULL,
	libelle_departement                             TEXT		NULL,
	ligne_acheminement                              TEXT		NULL,
	telephone                                       TEXT		NULL,
	telecopie                                       TEXT		NULL,
	categorie_etablissement                         TEXT		NULL,
	libelle_categorie_etablissement                  TEXT		NULL,
	categorie_agregat_etablissement                 TEXT		NULL,
	libelle_categorie_agregat_etablissement         TEXT		NULL,
	siret                                           TEXT		NULL,
	code_ape                                        TEXT		NULL,
	code_mft                                        TEXT		NULL,
	libelle_mft					TEXT		NULL,			
	code_sph                                        TEXT		NULL,
	libelle_SPH                                     TEXT		NULL,
	date_ouverture                                  TEXT		NULL,
	date_autorisation                               TEXT		NULL,
	date_maj_structure                              TEXT		NULL,
	numero_education_national                       TEXT		NULL
)                                                       
;                                                       
                                                        
DROP TABLE IF EXISTS finess_geo;                        
                                                        
CREATE TABLE finess_geo (
	finess_et					TEXT		NULL,
	coord_x						FLOAT		NULL,
	coord_y                                         FLOAT		NULL,
	source_coord                                    TEXT		NULL,
	date_maj                                        TEXT		NULL
) 
;

/************************

La base officielle des codes postaux est un jeu de données qui fournit la 
correspondance entre les codes postaux et les codes INSEE des communes, 
de France (métropole et DOM), des TOM, ainsi que de MONACO. 


https://datanova.laposte.fr/explore/dataset/laposte_hexasmal/information/?disjunctive.code_commune_insee&disjunctive.nom_de_la_commune&disjunctive.code_postal&disjunctive.ligne_5

 ************************/

DROP TABLE IF EXISTS laposte;

CREATE TABLE laposte (
	code_commune_insee 		 		TEXT		NULL,
	nom_commune 					TEXT		NULL,
	code_postal 				 	TEXT		NULL,
	ligne_5 					TEXT		NULL,
	libelle_acheminement 				TEXT		NULL,
	gps		 				TEXT		NULL
)
;

DROP TABLE IF EXISTS laposte_postal;

CREATE TABLE laposte_postal (
	code_postal 				 	TEXT		NULL,
	gps		 				TEXT		NULL
)
;

DROP TABLE IF EXISTS laposte_commune;

CREATE TABLE laposte_commune (
	code_commune_insee 				TEXT		NULL,
	gps		 				TEXT		NULL
)
;

/************************

Derived tables from csv

 ************************/


DROP TABLE IF EXISTS care_site;

CREATE TABLE care_site (
	care_site_id						INTEGER		NOT NULL,
	care_site_uuid					uuid 		NOT NULL,

	finess_et					TEXT		NULL,
	finess_ej                                       TEXT		NULL,
	raison_sociale                                  TEXT		NULL,
	raison_sociale_longue                           TEXT		NULL,
	complement_raison_sociale                       TEXT		NULL,
	complement_distribution                         TEXT		NULL,
	numero_voie					TEXT		NULL,
	type_voie                                       TEXT		NULL,
	libelle_voie                                    TEXT		NULL,
	complement_voie                                 TEXT		NULL,
	lieu_dit_bp                                     TEXT		NULL,
	code_commune                                    TEXT		NULL,
	departement_concept_id				INTEGER		NULL,
	--code_departement				TEXT		NULL,
	--libelle_departement                             TEXT		NULL,
	ligne_acheminement                              TEXT		NULL,
	code_postal                              	TEXT		NULL,
	commune                              		TEXT		NULL,
	telephone                                       TEXT		NULL,
	telecopie                                       TEXT		NULL,
	categorie_etablissement_concept_id		INTEGER		NULL,
	--code_categorie_etablissement                     TEXT		NULL,
	--libelle_categorie_etablissement                  TEXT		NULL,
	categorie_agregat_etablissement_concept_id	INTEGER		NULL,
	--code_categorie_agregat_etablissement            TEXT		NULL,
	--libelle_categorie_agregat_etablissement         TEXT		NULL,
	siret                                           TEXT		NULL,
	code_ape                                        TEXT		NULL,
	mft_concept_id				INTEGER		NULL,
	-- code_mft                                        TEXT		NULL,
	-- libelle_mft					TEXT		NULL,			
	sph_concept_id				INTEGER		NULL,
	-- code_sph                                        TEXT		NULL,
	-- libelle_SPH                                     TEXT		NULL,
	date_ouverture                                  TEXT		NULL,
	date_autorisation                               TEXT		NULL,
	date_maj_structure                              TEXT		NULL,
	numero_education_national                       TEXT		NULL,

	lambert_coord_x					FLOAT		NULL,
	lambert_coord_y                         	FLOAT       	NULL,

	wsg_coord_x					FLOAT 		NULL,
	wsg_coord_y					FLOAT		NULL,

	code_commune_coord_x                      	FLOAT		NULL,
	code_commune_coord_y                       	FLOAT		NULL,

	code_postal_coord_x                      	FLOAT		NULL,
	code_postal_coord_y                       	FLOAT		NULL,

	insert_datetime					TIMESTAMP 	NOT NULL
);

/************************

FINESS Extraction du Fichier des établissements 

Liste des établissements du domaine sanitaire et social.
Informations sur la Géo-localisation : Le système d’information source contenant les coordonnées géographiques permettant de géo-localiser les établissements

https://www.data.gouv.fr/fr/datasets/finess-extraction-du-fichier-des-etablissements/#_

 ************************/


