/*********************************************************************************
#
#
#     Licence
#
#
#
 ********************************************************************************/

/************************

####### #######  ####### ######     #    ######
   #    #     #  #     # #     #    #    #     #
   #    #     #  #     # #     #    #    #     #
   #    #     #  #     # ######     #    ######
   #    #     #  #     # #     #    #    #     #
   #    #     #  #     # #     #    #    #     #
   #    #######  ####### ######     #    ######

last revised: 04-April-2021

Authors:  Adrien PARROT
Authors:  Quentin PARROT

 *************************/

/************************

DROP index
TRUNCATE TABLE

*************************/

TRUNCATE TABLE care_site CASCADE;

/************************

FILL API TABLES

*************************/

-- care_site
INSERT INTO care_site (
	finess_et					
	, finess_ej                                       
	, raison_sociale                                  
	, raison_sociale_longue                           
	, complement_raison_sociale                       
	, complement_distribution                         
	, numero_voie					
	, type_voie                                       
	, libelle_voie                                    
	, complement_voie                                 
	, lieu_dit_bp                                     
	, code_commune                                     
	, departement_concept_id
	-- , code_departement					
	-- , libelle_departement                             
	, ligne_acheminement                              
	, code_postal
	, commune
	, telephone                                       
	, telecopie                                       
	, categorie_etablissement_concept_id
	--, code_categorie_etablissement                         
	--, libelle_categorie_etablissement                  
	, categorie_agregat_etablissement_concept_id
	--, code_categorie_agregat_etablissement                 
	--, libelle_categorie_agregat_etablissement         
	, siret                                           
	, code_ape                                        
	, mft_concept_id
	-- , code_mft                                        
	-- , libelle_mft					
	, sph_concept_id
	--, code_sph                                        
	--, libelle_SPH                                     
	, date_ouverture                                  
	, date_autorisation                               
	, date_maj_structure                              
	, numero_education_national                       

	, lambert_coord_x					
	, lambert_coord_y                         	

	, wsg_coord_x						
	, wsg_coord_y						

	, code_commune_coord_x		
	, code_commune_coord_y			

	, code_postal_coord_x		
	, code_postal_coord_y			
)
SELECT DISTINCT fs.finess_et					
	, fs.finess_ej                                       
	, fs.raison_sociale                                  
	, fs.raison_sociale_longue                           
	, fs.complement_raison_sociale                       
	, fs.complement_distribution                         
	, fs.numero_voie					
	, fs.type_voie                                       
	, fs.libelle_voie                                    
	, fs.complement_voie                                 
	, fs.lieu_dit_bp                                     
	, fs.code_commune                                     
	, departement.concept_id
	-- , fs.departement					
	-- , fs.libelle_departement                             
	, fs.ligne_acheminement                              
	, location.zip
	, location.city
	, fs.telephone                                       
	, fs.telecopie                                       
	, cat_et.concept_id
	-- , fs.categorie_etablissement                         
	-- , fs.libelle_categorie_etablissement                  
	, cat_ag_et.concept_id
	-- , fs.categorie_agregat_etablissement                 
	-- , fs.libelle_categorie_agregat_etablissement         
	, fs.siret                                           
	, fs.code_ape                                        
	, mft.concept_id
	-- , fs.code_mft                                        
	-- , fs.libelle_mft					
	, sph.concept_id
	-- , fs.code_sph                                        
	-- , fs.libelle_SPH                                     
	, fs.date_ouverture                                  
	, fs.date_autorisation                               
	, fs.date_maj_structure                              
	, fs.numero_education_national                       

	, fg.coord_x 					
	, fg.coord_y                          	

	, location.latitude 					
	, location.longitude					
	
	, split_part(poste_cm.gps, ',', 1)::float                
	, split_part(poste_cm.gps, ',', 2)::float                

	, split_part(poste_cp.gps, ',', 1)::float                
	, split_part(poste_cp.gps, ',', 2)::float                

FROM finess_struct fs
	LEFT JOIN omop.concept departement ON (fs.departement = departement.concept_code) AND departement.vocabulary_id = 'TRE-G09-DepartementOM'
	LEFT JOIN omop.concept cat_et ON (fs.categorie_etablissement = cat_et.concept_code) AND cat_et.vocabulary_id = 'TRE-R66-CategorieEtablissement'
	LEFT JOIN omop.concept cat_ag_et ON (fs.categorie_agregat_etablissement = cat_ag_et.concept_code) AND cat_ag_et.vocabulary_id = 'TRE-R65-AgregatCategorieEtablissement'
	LEFT JOIN omop.concept mft ON (fs.code_mft = mft.concept_code) AND mft.vocabulary_id = 'TRE-R74-ModeFixationTarifaire'
	LEFT JOIN omop.concept sph ON (fs.code_sph = sph.concept_code) AND sph.vocabulary_id = 'TRE-R73-ESPIC-FHIR'
LEFT JOIN finess_geo fg ON (fs.finess_et = fg.finess_et) 			-- finess_et is unique in finess_geo table
LEFT JOIN omop.concept c ON (fs.finess_et = c.concept_code) 
	LEFT JOIN omop.fact_relationship fr ON (c.concept_id = fr.fact_id_2 and domain_concept_id_2 = 58) -- 58 Type Concept
	LEFT JOIN omop.care_site cs ON (fr.fact_id_1 = cs.care_site_id)
	LEFT JOIN omop.location ON (cs.location_id = location.location_id)
LEFT JOIN laposte_commune poste_cm ON (fs.code_commune = poste_cm.code_commune_insee)
LEFT JOIN laposte_postal poste_cp ON (location.zip = poste_cp.code_postal)
;



/************************

Create Index

*************************/

-- CREATE INDEX idx_diplome_inpp ON diplome  (inpp ASC);
