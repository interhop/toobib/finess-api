/*********************************************************************************
#
#
#     Licence
#
#
#
 ********************************************************************************/

/************************

####### #######  ####### ######     #    ######
   #    #     #  #     # #     #    #    #     #
   #    #     #  #     # #     #    #    #     #
   #    #     #  #     # ######     #    ######
   #    #     #  #     # #     #    #    #     #
   #    #     #  #     # #     #    #    #     #
   #    #######  ####### ######     #    ######

last revised: 04-Feb-2021

Authors:  Adrien PARROT
Authors:  Quentin PARROT

 *************************/

/************************

INSEE Laposte

************************/

TRUNCATE TABLE laposte CASCADE;
DROP INDEX IF EXISTS idx_laposte_code_commune;
DROP INDEX IF EXISTS idx_laposte_code_postal;

\copy laposte FROM 'download/laposte.csv' WITH DELIMITER '|' CSV HEADER ENCODING 'utf8' ;


DROP INDEX IF EXISTS idx_laposte_commune_code_commune;
INSERT INTO laposte_commune (
	code_commune_insee
	, gps

)
WITH tmp AS (
	SELECT *,
	row_number() over (partition by code_commune_insee order by code_commune_insee asc) as rn
	from laposte
)
SELECT code_commune_insee, gps
FROM tmp
where rn = 1
;

DROP INDEX IF EXISTS idx_laposte_postal_code_postal;
INSERT INTO laposte_postal (
	code_postal
	, gps
)
WITH tmp AS (
	SELECT *,
	row_number() over (partition by code_postal order by code_postal asc) as rn
	from laposte
)
SELECT code_postal, gps
FROM tmp
where rn = 1
;


/************************

FINESS

************************/

TRUNCATE TABLE finess_struct CASCADE;
DROP INDEX IF EXISTS idx_finess_struct_finess_et;

\copy finess_struct FROM 'download/finess_struct.csv' WITH DELIMITER ';' CSV ENCODING 'latin1';


TRUNCATE TABLE finess_geo CASCADE;
DROP INDEX IF EXISTS idx_finess_geo_finess_et;

\copy finess_geo FROM 'download/finess_geo.csv' WITH DELIMITER ';' CSV ENCODING 'latin1';

/************************

Create Index

************************/

CREATE INDEX idx_laposte_code_commune  ON  laposte (code_commune_insee ASC);
CLUSTER laposte USING idx_laposte_code_commune;

CREATE INDEX  idx_laposte_postal_code_postal ON laposte_postal (code_postal ASC);
CLUSTER laposte_postal USING  idx_laposte_postal_code_postal;

CREATE INDEX  idx_laposte_commune_code_commune ON laposte_commune (code_commune_insee ASC);
CLUSTER laposte_commune USING  idx_laposte_commune_code_commune;

CREATE INDEX idx_finess_struct_finess_et  ON  finess_struct (finess_et ASC);
CLUSTER finess_struct USING idx_finess_struct_finess_et;

CREATE INDEX idx_finess_geo_finess_et  ON  finess_geo (finess_et ASC);
CLUSTER finess_geo USING idx_finess_geo_finess_et;
