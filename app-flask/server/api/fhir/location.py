from ...models.care_site import CareSiteModel
from ...models.materialized_view import  CareSiteSearchModel
from .datatypes import HumanName, Identifier, ContactPoint, Address, CodeableConcept, Extension


LOCATION_IDENTIFIER_SYSTEM = "https://toobib.org/location_identifier"

"""

https://www.hl7.org/fhir/location.html

{doco
  "resourceType" : "Location",
  // from Resource: id, meta, implicitRules, and language
  // from DomainResource: text, contained, extension, and modifierExtension
  "identifier" : [{ Identifier }], // Unique code or number identifying the location to its users
  "status" : "<code>", // active | suspended | inactive
  "operationalStatus" : { Coding }, // The operational status of the location (typically only for a bed/room)
  "name" : "<string>", // Name of the location as used by humans
  "alias" : ["<string>"], // A list of alternate names that the location is known as, or was known as, in the past
  "description" : "<string>", // Additional details about the location that could be displayed as further information to identify the location beyond its name
  "mode" : "<code>", // instance | kind
  "type" : [{ CodeableConcept }], // Type of function performed
  "telecom" : [{ ContactPoint }], // Contact details of the location
  "address" : { Address }, // Physical location
  "physicalType" : { CodeableConcept }, // Physical form of the location
  "position" : { // The absolute geographic location
    "longitude" : <decimal>, // R!  Longitude with WGS84 datum
    "latitude" : <decimal>, // R!  Latitude with WGS84 datum
    "altitude" : <decimal> // Altitude with WGS84 datum
  },
  "managingOrganization" : { Reference(Organization) }, // Organization responsible for provisioning and upkeep
  "partOf" : { Reference(Location) }, // Another Location this one is physically a part of
  "hoursOfOperation" : [{ // What days/times during a week is this location usually open
    "daysOfWeek" : ["<code>"], // mon | tue | wed | thu | fri | sat | sun
    "allDay" : <boolean>, // The Location is open all day
    "openingTime" : "<time>", // Time that the Location opens
    "closingTime" : "<time>" // Time that the Location closes
  }],
  "availabilityExceptions" : "<string>", // Description of availability exceptions
  "endpoint" : [{ Reference(Endpoint) }] // Technical endpoints providing access to services operated for the location
}


"""

class LocationFhir:
    def __init__(self):
        self.id = ""
        self.identifier = [] 
        self.status = "active",
        self.name = ""
        self.alias = []
        self.type = []
        self.telecom = []
        self.address = []
        self.position = None

    def json(self):
        return {
            "resourceType": "Location",
            "id": str(self.id),
            "identifier": [i.json() for i in self.identifier],
            "status": self.status,
            "name": self.name,
            "alias": self.alias,
            "type": [t.json() for t in self.type],
            "telecom": [t.json() for t in self.telecom],
            "address": [a.json() for a in self.address],
            "position": self.position,
        }

 
    @classmethod
    def find_by_uuid(cls, uuid):
        """
            return ONE location ressource
        """
        return cls.__get_from_db(CareSiteModel.find_by_uuid(uuid))

    @classmethod
    def find_from_payload(cls, data):
        if 'identifier' in data:
            identifiers = data['identifier'].split('|')
            if len(identifiers) != 2:
                return None, 'When provided, the identifier query parameter must include both a system and a code.'
            if identifiers[0].lower() not in ['finess_et', 'siret']:
                return None, "When provided, the system of identifier query parameter must be 'finess_et' or 'siret'"
            data[identifiers[0]] = identifiers[1]
            del data['identifier']
        if 'type' in data:
            types = data['type'].split('|')
            if len(types) != 2:
                return None, 'When provided, the type query parameter must include both a system and a code.'
            if types[0] not in ['TRE-R66-CategorieEtablissement']:
                return None, "When provided, the system of identifier query parameter must be 'TRE-R66-CategorieEtablissement'"
            data['categorie_etablissement'] = types[1]
            del data['type']
        if 'french-department' in data:
            types = data['french-department'].split('|')
            if len(types) != 2:
                return None, 'When provided, the type query parameter must include both a system and a code.'
            if types[0] not in ['TRE-G09-DepartementOM']:
                return None, "When provided, the system of identifier query parameter must be 'TRE-G09-DepartementOM'"
            data['departement'] = types[1]
            del data['french-department']

        data, page, limit = CareSiteSearchModel.normalize_check_payload(data)
        if page <= 0:
            return None, 'Page must be a positive integer'
        if limit <= 0:
            return None, '_count must be a positive integer'
        elif limit > 1000:
            return None, '_count must be <= 1000'


        search = CareSiteSearchModel.search(
                data,
                page=page,
                limit=limit
        )

        if search.total == 0:
            return None, 'Practitioner not found'

        search.locations = []
        for s in search.items:
            search.locations += cls.__get_from_search(s)

        return search, None


    @classmethod
    def __get_from_search(cls, search):
        """ 
            return a list
        """
        orga = cls.__get_from_db(CareSiteModel.find_by_id(search.care_site_id))
        return [orga]


    @classmethod
    def __get_from_db(cls, care_site):
        """
            There are no name, alias and type because it is questionned by finess table
        """
        if not care_site:
            return None

        orga = cls()
        orga.id = care_site.uuid
        orga.identifier = cls.__get_from_db_identifier(care_site)
        orga.name = care_site.raison_sociale if care_site.raison_sociale else ""
        orga.alias = cls.__get_from_db_alias(care_site)
        orga.type = cls.__get_from_db_type(care_site)
        orga.telecom = cls.__get_from_db_telecom(care_site)
        orga.address = cls.__get_from_db_address(care_site)
        orga.position = cls.__get_from_db_position(care_site)

        return orga

    @classmethod
    def __get_from_db_identifier(cls, care_site):
        ret = []
        param = {
            "description_system": LOCATION_IDENTIFIER_SYSTEM,
            "description_code": "",
            "value_system": "",
            "value_value": ""
        }

        if care_site.finess_et: # in fact never null
            param['description_code'] = 'FINESS'
            param['value_system'] = LOCATION_IDENTIFIER_SYSTEM + '/finess_et'
            param['value_value'] = care_site.finess_et
            ret.append(Identifier(**param))
        return ret

    @classmethod
    def __get_from_db_type(cls, care_site):
        if care_site.categorie_etablissement_concept_id:
            return [ CodeableConcept(
                        system=care_site.categorie_etablissement.vocabulary_id, 
                        code=care_site.categorie_etablissement.concept_code, 
                        display=care_site.categorie_etablissement.concept_name, 
            )]
        return []


    @classmethod
    def __get_from_db_alias(cls, care_site):
        if care_site.complement_raison_sociale:
            return [care_site.complement_raison_sociale]
        return []

 
    @classmethod
    def __get_from_db_telecom(cls, care_site):
        ret = []

        if care_site.telephone:
            ret.append(ContactPoint(system="phone", value=care_site.telephone))
        if care_site.telecopie:
            ret.append(ContactPoint(system="fax", value=care_site.telecopie))
        
        return ret

    @classmethod
    def __get_from_db_address(cls, care_site):
        ret = []
        if care_site.numero_voie is not None  \
            or care_site.type_voie is not None \
            or care_site.libelle_voie is not None  \
            or care_site.commune is not None \
            or care_site.code_postal is not None:

            address = Address(
                line = [ str(care_site.numero_voie or ''), (str(care_site.type_voie or '') + ' ' +  str(care_site.libelle_voie or '')).strip() ],
                city = str(care_site.commune or ''),
                postal_code = str(care_site.code_postal or ''),
                extension = [
                    Extension(
                        url="https://apifhir.annuaire.sante.fr/exposed/structuredefinition/french-department",
                        value=CodeableConcept(
                            system=care_site.departement.vocabulary_id,
                            code=care_site.departement.concept_code,
                            display=care_site.departement.concept_name,
                        )
                    )
                ]
            )
            ret.append(address)

        return ret

    @classmethod
    def __get_from_db_position(cls, care_site):
        return {
                "longitude": care_site.wsg_coord_y,
                "latitude": care_site.wsg_coord_x,
            }

