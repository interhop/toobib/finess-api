# DataTypes FHIR documentation : https://www.hl7.org/fhir/datatypes.html

class HumanName:
    """
    https://www.hl7.org/fhir/datatypes.html#HumanName
    
    {
      "use" : "<code>", // usual | official | temp | nickname | anonymous | old | maiden
      "text" : "<string>", // Text representation of the full name
      "family" : "<string>", // Family name (often called 'Surname')
      "given" : ["<string>"], // Given names (not always 'first'). Includes middle names
      "prefix" : ["<string>"], // Parts that come before the name
      "suffix" : ["<string>"], // Parts that come after the name
      "period" : { Period } // Time period when name was/is in use
    }
    """
    def __init__(self, family, given, use="official", prefix=[], suffix=[], period=None):
        self.use = use
        self.text = self.text_builder(family, given, prefix, suffix)
        self.family = family
        self.given = given
        self.prefix = prefix
        self.suffix = suffix
        self.period = period

    def text_builder(self, family, given, prefix, suffix):
        ret = ""
        if len(prefix) != 0:
           ret += ' '.join(filter(None, prefix)) 
        if len(given) != 0:
           ret += ' ' + ' '.join(filter(None, given))
        if family:
            ret += ' ' + family
        if len(suffix) != 0:
           ret += ' ' + ' '.join(filter(None, suffix)) 

        return ret.strip()

    def json(self):
        return {
            'use': self.use,
            'text': self.text, 
            'family': self.family, 
            'given': self.given,
            'prefix': self.prefix,
            'suffix': self.suffix, 
            'period': self.period, 
        }

        

class Address:
    """
    https://www.hl7.org/fhir/datatypes.html#Address
    
    {
      "use" : "<code>", // home | work | temp | old | billing - purpose of this address
      "type" : "<code>", // postal | physical | both
      "text" : "<string>", // Text representation of the address
      "line" : ["<string>"], // Street name, number, direction & P.O. Box etc.
      "city" : "<string>", // Name of city, town etc.
      "district" : "<string>", // District name (aka county)
      "state" : "<string>", // Sub-unit of country (abbreviations ok)
      "postalCode" : "<string>", // Postal code for area
      "country" : "<string>", // Country (e.g. can be ISO 3166 2 or 3 letter code)
      "period" : { Period } // Time period when address was/is in use
    }
    """
    def __init__(self, line, city, postal_code, type="postal", use="work", district="", state="", country="", period=None, extension=[]):
        self.use = use
        self.type = type
        self.text = self.text_builder(line, city, postal_code, district, state, country)
        self.line = line
        self.city = city if city else ""
        self.postal_code = postal_code if postal_code else ""
        self.district = district
        self.state = state
        self.country = country
        self.period = period
        self.extension = extension

    def text_builder(self, line, city, postal_code, district="", state="", country=""):
        ret = ""

        if len(line) != 0:
           ret += ' '.join(filter(None, line)) 

        ret +=  ' ' + city if city else "" + ' ' + postal_code if postal_code else ""
        if district != "":
            ret += ' ' + district
        if state != "":
            ret += ' ' + state
        if country != "":
            ret += ' ' + country
        return ret


    def json(self):
        return  {
            'use': self.use,
            'type': self.type,
            'text': self.text, 
            'line': self.line, 
            'city': self.city,
            'postal_code': self.postal_code, 
            'district': self.district, 
            'state': self.state, 
            'country': self.country, 
            'period': self.period, 
            'extension': [e.json() for e in self.extension]
        }

class ContactPoint:
    """
    https://www.hl7.org/fhir/datatypes.html#ContactPoint
    
    {doco
      "system" : "<code>", // C? phone | fax | email | pager | url | sms | other
      "value" : "<string>", // The actual contact point details
      "use" : "<code>", // home | work | temp | old | mobile - purpose of this contact point
      "rank" : "<positiveInt>", // Specify preferred order of use (1 = highest)
      "period" : { Period } // Time period when the contact point was/is in use
    }
    """
    def __init__(self, system, value, use="work", rank=1, period=None):
        self.system = system
        self.value = value
        self.use = use
        self.rank = rank
        self.period = period

    def json(self):
        return {
            'system': self.system,
            'value': self.value, 
            'use': self.use, 
            'rank': self.rank,
            'period': self.period, 
        }

class Identifier:

    """
    https://www.hl7.org/fhir/datatypes.html#Identifier
    
    {doco
      "use" : "<code>", // usual | official | temp | secondary | old (If known)
      "type" : { CodeableConcept }, // Description of identifier
      "system" : "<uri>", // The namespace for the identifier value
      "value" : "<string>", // The value that is unique
      "period" : { Period }, // Time period when id is/was valid for use
      "assigner" : { Reference(Organization) } // Organization that issued id (may be just text)
    }
    """
    def __init__(self, description_system, description_code, value_system="", value_value="", use="official", period=None):
        self.use = use
        self.type = CodeableConcept(description_system, description_code)
        self.system = value_system
        self.value = value_value
        self.period = period

    def json(self):
        return {
            'use': self.use,
            'type': self.type.json(), 
            'system': self.system, 
            'value': self.value, 
            'period': self.period, 
        }

class CodeableConcept:
    """
    https://www.hl7.org/fhir/datatypes.html#CodeableConcept

    {doco
      // from Element: extension
      "coding" : [{ Coding }], // Code defined by a terminology system
      "text" : "<string>" // Plain text representation of the concept
    }


    	"valueCodeableConcept": {
		"coding": [
			{
				"system": "http://snomed.info/sct",
				"code": "260385009",
				"display": "Negative"
			}, {
				"system": "https://acme.lab/resultcodes",
				"code": "NEG",
				"display": "Negative"
			}
		],
		"text": "Negative for Chlamydia Trachomatis rRNA"
	}

    """

    def __init__(self, system, code, display=""):
        self.coding = [ Coding(system, code, display) ]
        self.text = display

    def json(self):
        return {
            'coding': [c.json() for c in self.coding],
            'text': self.text,
        }
 
class Coding:

    """
    https://www.hl7.org/fhir/datatypes.html#Coding
    
    {doco
      // from Element: extension
      "system" : "<uri>", // Identity of the terminology system
      "version" : "<string>", // Version of the system - if relevant
      "code" : "<code>", // Symbol in syntax defined by the system
      "display" : "<string>", // Representation defined by the system
      "userSelected" : <boolean> // If this coding was chosen directly by the user
    } 
    """

    def __init__(self, system, code, display):
        self.system = system
        self.code = code
        self.display = display

    def json(self):
        return {
            'system': self.system,
            'code': self.code,
            'display': self.display,
        }

class Extension:

    """
    https://www.hl7.org/fhir/extensibility.html 
    example : https://www.hl7.org/fhir/basic-example.json.html

    {doco
      // from Element: extension
      "url" : "<uri>", // R!  identifies the meaning of the extension
      // value[x]: Value of extension. One of these 50:
      "valueBase64Binary" : "<base64Binary>"
      "valueBoolean" : <boolean>
      "valueCanonical" : "<canonical>"
      "valueCode" : "<code>"
      "valueDate" : "<date>"
      "valueDateTime" : "<dateTime>"
      "valueDecimal" : <decimal>
      "valueId" : "<id>"
      "valueInstant" : "<instant>"
      "valueInteger" : <integer>
      "valueMarkdown" : "<markdown>"
      "valueOid" : "<oid>"
      "valuePositiveInt" : "<positiveInt>"
      "valueString" : "<string>"
      "valueTime" : "<time>"
      "valueUnsignedInt" : "<unsignedInt>"
      "valueUri" : "<uri>"
      "valueUrl" : "<url>"
      "valueUuid" : "<uuid>"
      "valueAddress" : { Address }
      "valueAge" : { Age }
      "valueAnnotation" : { Annotation }
      "valueAttachment" : { Attachment }
      "valueCodeableConcept" : { CodeableConcept }
      "valueCoding" : { Coding }
      "valueContactPoint" : { ContactPoint }
      "valueCount" : { Count }
      "valueDistance" : { Distance }
      "valueDuration" : { Duration }
      "valueHumanName" : { HumanName }
      "valueIdentifier" : { Identifier }
      "valueMoney" : { Money }
      "valuePeriod" : { Period }
      "valueQuantity" : { Quantity }
      "valueRange" : { Range }
      "valueRatio" : { Ratio }
      "valueReference" : { Reference }
      "valueSampledData" : { SampledData }
      "valueSignature" : { Signature }
      "valueTiming" : { Timing }
      "valueContactDetail" : { ContactDetail }
      "valueContributor" : { Contributor }
      "valueDataRequirement" : { DataRequirement }
      "valueExpression" : { Expression }
      "valueParameterDefinition" : { ParameterDefinition }
      "valueRelatedArtifact" : { RelatedArtifact }
      "valueTriggerDefinition" : { TriggerDefinition }
      "valueUsageContext" : { UsageContext }
      "valueDosage" : { Dosage }
      "valueMeta" : { Meta }
    }

    """

    def __init__(self, url, value):
        self.url = url
        self.value = value

    def json(self):
        ret = {
            'url': self.url,
        }
        if isinstance(self.value, CodeableConcept):
            ret['valueCodeableConcept'] = self.value.json()
        return ret

"""
https://www.hl7.org/fhir/datatypes.html#Period

{
  "start" : "<dateTime>", // C? Starting time with inclusive boundary
  "end" : "<dateTime>" // C? End time with inclusive boundary, if not ongoing
}
"""
