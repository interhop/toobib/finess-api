from flask import request
from .organization import OrganizationFhir
from .location import LocationFhir


API_URL = 'https://finess.dev.api.interhop.org'
ORGANIZATION_URL = "Organization"
LOCATION_URL = "Location"

"""

https://www.hl7.org/fhir/bundle.html

{doco
  "resourceType" : "Bundle",
  // from Resource: id, meta, implicitRules, and language
  "identifier" : { Identifier }, // Persistent identifier for the bundle
  "type" : "<code>", // R!  document | message | transaction | transaction-response | batch | batch-response | history | searchset | collection
  "timestamp" : "<instant>", // When the bundle was assembled
  "total" : "<unsignedInt>", // C? If search, the total number of matches
  "link" : [{ // Links related to this Bundle
    "relation" : "<string>", // R!  See http://www.iana.org/assignments/link-relations/link-relations.xhtml#link-relations-1
    "url" : "<uri>" // R!  Reference details for the link
  }],
  "entry" : [{ // Entry in the bundle - will have a resource or information
    "link" : [{ Content as for Bundle.link }], // Links related to this entry
    "fullUrl" : "<uri>", // URI for resource (Absolute URL server address or URI for UUID/OID)
    "resource" : { Resource }, // A resource in the bundle
    "search" : { // C? Search related information
      "mode" : "<code>", // match | include | outcome - why this is in the result set
      "score" : <decimal> // Search ranking (between 0 and 1)
    },
    "request" : { // C? Additional execution information (transaction/batch/history)
      "method" : "<code>", // R!  GET | HEAD | POST | PUT | DELETE | PATCH
      "url" : "<uri>", // R!  URL for HTTP equivalent of this entry
      "ifNoneMatch" : "<string>", // For managing cache currency
      "ifModifiedSince" : "<instant>", // For managing cache currency
      "ifMatch" : "<string>", // For managing update contention
      "ifNoneExist" : "<string>" // For conditional creates
    },
    "response" : { // C? Results of execution (transaction/batch/history)
      "status" : "<string>", // R!  Status response code (text optional)
      "location" : "<uri>", // The location (if the operation returns a location)
      "etag" : "<string>", // The Etag for the resource (if relevant)
      "lastModified" : "<instant>", // Server's date time modified
      "outcome" : { Resource } // OperationOutcome with hints and warnings (for batch/transaction)
    }
  }],
  "signature" : { Signature } // Digital Signature
}

"""

class BundleFhir:
    def __init__(self, type="searchset"):
        self.type = type
        self.total = 1
        self.link = []
        if type == "searchset":
            self.entry = []

    def json(self):
        ret = {
            "resourceType": "bundle",
            "type": self.type,
            "total": self.total,
            "link": self.link,
        }
        if hasattr(self, 'entry'):
            ret_entry = []
            for e in self.entry:
                ret_entry.append({'fullURL': e['fullURL'], 
                                'resource': e['resource'].json()})
            ret["entry"] = ret_entry

        return ret

 
    @classmethod
    def get_searchset(cls, ressources, url_params, search=None):
        """
            search is a Paginate Object : https://flask-sqlalchemy.palletsprojects.com/en/2.x/api/#flask_sqlalchemy.Pagination
        """
        bundle = cls(type="searchset")
        bundle.total = search.total if search else 1
        bundle.link = cls.__build_pagination(url_params, search)
        bundle.entry = cls.__build_entries(ressources)

        return bundle

    @classmethod
    def __build_pagination(cls, url_params={}, search=None):
        url_params['page'] = search.page if search else 1
        ret = [
            {
                "relation": "self",
                "url": API_URL + request.path + '?' + '&'.join(key + '=' + str(val) for key, val in url_params.items())
            }
        ]
        if search and search.prev_num:
            url_params['page'] = search.prev_num
            ret.append(
                {
                    "relation": "previous",
                    "url": API_URL + request.path + '?' + '&'.join(key + '=' + str(val) for key, val in url_params.items())
                }

            )
        if search and search.next_num:
            url_params['page'] = search.next_num
            ret.append(
                {
                    "relation": "next",
                    "url": API_URL + request.path + '?' + '&'.join(key + '=' + str(val) for key, val in url_params.items())
                }

            )

        url_params['page'] = 1
        ret.append(
            {
                "relation": "first",
                "url": API_URL + request.path + '?' + '&'.join(key + '=' + str(val) for key, val in url_params.items())
            }

        )
        url_params['page'] = search.pages if search else 1
        ret.append(
            {
                "relation": "last",
                "url": API_URL + request.path + '?' + '&'.join(key + '=' + str(val) for key, val in url_params.items())
            }

        )

        return ret 


    @classmethod
    def __build_entries(cls, ressources):
        ret = []
        for r in ressources:
            if isinstance(r, OrganizationFhir):
                ret.append({'fullURL': API_URL + ORGANIZATION_URL + '/' + str(r.id),
                    'resource': r})
            elif isinstance(r, LocationFhir):
                ret.append({'fullURL': API_URL + LOCATION_URL + '/' + str(r.id),
                    'resource': r})
        return ret

