from ...models.care_site import CareSiteModel
from ...models.materialized_view import  CareSiteSearchModel
from .datatypes import HumanName, Identifier, ContactPoint, Address, CodeableConcept, Extension


ORGANIZATION_IDENTIFIER_SYSTEM = "https://toobib.org/organization_identifier"

"""

https://www.hl7.org/fhir/organization.html

{doco
  "resourceType" : "Organization",
  // from Resource: id, meta, implicitRules, and language
  // from DomainResource: text, contained, extension, and modifierExtension
  "identifier" : [{ Identifier }], // C? Identifies this organization  across multiple systems
  "active" : <boolean>, // Whether the organization's record is still in active use
  "type" : [{ CodeableConcept }], // Kind of organization
  "name" : "<string>", // C? Name used for the organization
  "alias" : ["<string>"], // A list of alternate names that the organization is known as, or was known as in the past
  "telecom" : [{ ContactPoint }], // C? A contact detail for the organization
  "address" : [{ Address }], // C? An address for the organization
  "partOf" : { Reference(Organization) }, // The organization of which this organization forms a part
  "contact" : [{ // Contact for the organization for a certain purpose
    "purpose" : { CodeableConcept }, // The type of contact
    "name" : { HumanName }, // A name associated with the contact
    "telecom" : [{ ContactPoint }], // Contact details (telephone, email, etc.)  for a contact
    "address" : { Address } // Visiting or postal addresses for the contact
  }],
  "endpoint" : [{ Reference(Endpoint) }] // Technical endpoints providing access to services operated for the organization
}
"""

class OrganizationFhir:
    def __init__(self):
        self.id = ""
        self.identifier = [] 
        self.active = False, ## external is inactive
        self.type = []
        self.name = ""
        self.alias = []
        self.telecom = []
        self.address = []

    def json(self):
        return {
            "resourceType": "Organization",
            "id": str(self.id),
            "identifier": [i.json() for i in self.identifier],
            "active": self.active,
            "type": [t.json() for t in self.type], # not in official
            "name": self.name,
            "alias": self.alias,
            "telecom": [t.json() for t in self.telecom],
            "address": [a.json() for a in self.address],
        }

 
    @classmethod
    def find_by_uuid(cls, uuid):
        """
            return ONE organization ressource
        """
        return cls.__get_from_db(CareSiteModel.find_by_uuid(uuid))

    @classmethod
    def find_from_payload(cls, data):
        if 'identifier' in data:
            identifiers = data['identifier'].split('|')
            if len(identifiers) != 2:
                return None, 'When provided, the identifier query parameter must include both a system and a code.'
            if identifiers[0].lower() not in ['finess_ej', 'siret']:
                return None, "When provided, the system of identifier query parameter must be 'finess_ej' or 'siret'"
            data[identifiers[0]] = identifiers[1]
            del data['identifier']
        if 'type' in data:
            types = data['type'].split('|')
            if len(types) != 2:
                return None, 'When provided, the type query parameter must include both a system and a code.'
            if types[0] not in ['TRE-R66-CategorieEtablissement']:
                return None, "When provided, the system of identifier query parameter must be 'TRE-R66-CategorieEtablissement'"
            data['categorie_etablissement'] = types[1]
            del data['type']
        if 'french-department' in data:
            types = data['french-department'].split('|')
            if len(types) != 2:
                return None, 'When provided, the type query parameter must include both a system and a code.'
            if types[0] not in ['TRE-G09-DepartementOM']:
                return None, "When provided, the system of identifier query parameter must be 'TRE-G09-DepartementOM'"
            data['departement'] = types[1]
            del data['french-department']



        data, page, limit = CareSiteSearchModel.normalize_check_payload(data)
        if page <= 0:
            return None, 'Page must be a positive integer'
        if limit <= 0:
            return None, '_count must be a positive integer'
        elif limit > 1000:
            return None, '_count must be <= 1000'


        search = CareSiteSearchModel.search(
                data,
                page=page,
                limit=limit
        )

        if search.total == 0:
            return None, 'Practitioner not found'

        search.organizations = []
        for s in search.items:
            search.organizations += cls.__get_from_search(s)

        return search, None


    @classmethod
    def __get_from_search(cls, search):
        """ 
            return a list
        """
        orga = cls.__get_from_db(CareSiteModel.find_by_id(search.care_site_id))
        return [orga]


    @classmethod
    def __get_from_db(cls, care_site):
        """
            There are no name, alias and type because it is questionned by finess table
        """
        if not care_site:
            return None

        orga = cls()
        orga.id = care_site.uuid
        orga.identifier = cls.__get_from_db_identifier(care_site)
        orga.type = cls.__get_from_db_type(care_site)
        orga.name = care_site.raison_sociale if care_site.raison_sociale else ""
        orga.alias = cls.__get_from_db_alias(care_site)
        orga.telecom = cls.__get_from_db_telecom(care_site)
        orga.address = cls.__get_from_db_address(care_site)

        return orga

    @classmethod
    def __get_from_db_identifier(cls, care_site):
        ret = []
        param = {
            "description_system": ORGANIZATION_IDENTIFIER_SYSTEM,
            "description_code": "",
            "value_system": "",
            "value_value": ""
        }

        if care_site.finess_ej: # in fact never null
            param['description_code'] = 'FINESS'
            param['value_system'] = ORGANIZATION_IDENTIFIER_SYSTEM + '/finess_ej'
            param['value_value'] = care_site.finess_ej
            ret.append(Identifier(**param))
        if care_site.siret:
            param['description_code'] = 'SIRET'
            param['value_system'] = ORGANIZATION_IDENTIFIER_SYSTEM + '/siret'
            param['value_value'] = care_site.siret
            ret.append(Identifier(**param))

        return ret

    @classmethod
    def __get_from_db_type(cls, care_site):
        if care_site.categorie_etablissement_concept_id:
            return [ CodeableConcept(
                        system=care_site.categorie_etablissement.vocabulary_id, 
                        code=care_site.categorie_etablissement.concept_code, 
                        display=care_site.categorie_etablissement.concept_name, 
            )]
        return []


    @classmethod
    def __get_from_db_alias(cls, care_site):
        if care_site.complement_raison_sociale:
            return [care_site.complement_raison_sociale]
        return []

 
    @classmethod
    def __get_from_db_telecom(cls, care_site):
        ret = []

        if care_site.telephone:
            ret.append(ContactPoint(system="phone", value=care_site.telephone))
        if care_site.telecopie:
            ret.append(ContactPoint(system="fax", value=care_site.telecopie))
        
        return ret

    @classmethod
    def __get_from_db_address(cls, care_site):
        ret = []
        if care_site.numero_voie is not None  \
            or care_site.type_voie is not None \
            or care_site.libelle_voie is not None  \
            or care_site.commune is not None \
            or care_site.code_postal is not None:

            address = Address(
                line = [ str(care_site.numero_voie or ''), (str(care_site.type_voie or '') + ' ' +  str(care_site.libelle_voie or '')).strip() ],
                city = str(care_site.commune or ''),
                postal_code = str(care_site.code_postal or ''),
                extension = [ 
                    Extension(
                        url="https://apifhir.annuaire.sante.fr/exposed/structuredefinition/french-department", 
                        value=CodeableConcept(
                            system=care_site.departement.vocabulary_id, 
                            code=care_site.departement.concept_code, 
                            display=care_site.departement.concept_name, 
                        )
                    )
                ]
            )
            ret.append(address)
        
        return ret
