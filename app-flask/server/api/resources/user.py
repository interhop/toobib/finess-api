from flask_restful import Resource, reqparse
import werkzeug 
from flask_jwt_extended import create_access_token, \
		get_jwt, \
		get_jwt_identity, \
		jwt_required, \
		JWTManager

from datetime import datetime, timedelta
from ..models.toobib_models import UserModel
from ..blocklist import BLOCKLIST


class User(Resource):
    parser = reqparse.RequestParser()

    parser.add_argument('password', 
        type=str, required=True,
        help="you have to provide a password (str)"
    )
    parser.add_argument('email',
        type=str, required=True,
        help="you have to provide an email (str)"
    )

    @classmethod
    def get(cls, username):
        """
            Here are comments only available in the code.
            || Bellow is the description for swagger. This is written like yaml file. For more insight consult this example: https://editor.swagger.io/
            ---
            description: Perform a request that send the path to a file.
            tags:
                - User
            parameters:
                - in: path
                  name: path
                  required: true
                  schema:
                    type: string
            responses:
                200:
                    description: SUCCESS!
                404:
                    description: FAIL!
        """
        user = UserModel.find_by_username(username)
        if not user:
            return {'message': 'User not found'}, 404
        return user.registered(), 200

    @classmethod
    def post(cls, username):
        """
            Here are comments only available in the code.
            || Bellow is the description for swagger. This is written like yaml file. For more insight consult this example: https://editor.swagger.io/
            ---
            description: Perform a request that send the path to a file.
            tags:
                - User
            parameters:
                - in: path
                  name: path
                  required: true
                  schema:
                    type: string
            responses:
                200:
                    description: SUCCESS!
                404:
                    description: FAIL!
        """
        data = cls.parser.parse_args()

        if UserModel.find_by_username(username):
            return {'message': 'A user with that username already exists'}, 400
        if UserModel.find_by_email(data['email']):
            return {'message': 'This email already exists with another users!'}, 400

        user = UserModel(username, **data)
        user.upserting()
        return user.registered(), 201

    @classmethod
    def put(cls, username):
        """
            Here are comments only available in the code.
            || Bellow is the description for swagger. This is written like yaml file. For more insight consult this example: https://editor.swagger.io/
            ---
            description: Perform a request that send the path to a file.
            tags:
                - User
            parameters:
                - in: path
                  name: path
                  required: true
                  schema:
                    type: string
            responses:
                200:
                    description: SUCCESS!
                404:
                    description: FAIL!
        """
        data = User.parser.parse_args()

        user = UserModel.find_by_username(username)
        if not user: 
            user = UserModel(m_username, **data)
            user.upserting()
            return user.registered(), 2021
        else:
            for k in data:
                if k != "password":
                    setattr(user, k, data[k])
                if data['password']:
                    user.set_password(data['password'])
            user.upserting()
            return user.registered(), 200

class Login(Resource):
    parser = reqparse.RequestParser()
    parser.add_argument('username', type=str, required=True, help="Vous devez renseigner un nom d'utilisateur")
    parser.add_argument('password', type=str, required=True, help="Vous devez renseigner un mot de passe")

    @classmethod
    def post(cls):
        """
            Here are comments only available in the code.
            || Bellow is the description for swagger. This is written like yaml file. For more insight consult this example: https://editor.swagger.io/
            ---
            description: Perform a request that send the path to a file.
            tags:
                - User
            requestBody:
                description: login information
                required: true
                content:
                    application/json:
                        schema:
                            type: object
                            properties:
                                username:
                                    type: string
                                    default: user
                                password:
                                    type: string
                                    default: password
            responses:
                200:
                    description: SUCCESS!
                404:
                    description: FAIL!
        """
        data = cls.parser.parse_args()
        user = UserModel.find_by_username(data['username'])
        if user and user.check_password(data['password']) and user.invalid_reason != 'D': #TODO: method user_is_valid()
            expires = timedelta(days=1)
            access_token = create_access_token(identity=user, fresh=True, expires_delta=expires)
            return {
                    'access_token': access_token,
                    'user': user.registered(),
            }, 200
        return {"message": "Invalid credentials"}, 401

class Logout(Resource):
    @jwt_required() 
    def post(self):
        """
            Here are comments only available in the code.
            || Bellow is the description for swagger. This is written like yaml file. For more insight consult this example: https://editor.swagger.io/
            ---
            description: Perform a request that send the path to a file.
            tags:
                - User
            security:
                - Bearer: []
            responses:
                200:
                    description: SUCCESS!
                404:
                    description: FAIL!
        """
        jti = get_jwt()['jti'] # jti is "JWT ID", a unique identifier for a JWT.
        BLOCKLIST.add(jti)
        return {"message": "Successfully logged out"}, 200

