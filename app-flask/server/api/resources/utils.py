from flask import send_file, request, Blueprint
from spectree import Response

from server.models.materialized_view import CareSiteSearchModel
from server.models.care_site import CareSiteModel

from server.schema.spec import spectree_api
from server.schema.query.utils import WindowQuery, GeolocQuery

utils = Blueprint("Utils", __name__)

@utils.route("/window", methods=["GET"])
@spectree_api.validate(query=WindowQuery, resp=Response("HTTP_404"), tags=["Utils"])
def get():
    data = request.args.to_dict(flat=True)
    if 'method' not in data or data['method'] not in ['count']:
        return {'message': 'Method should be count'}, 404
    if 'category' not in data or data['category'] not in ['department', 'city']:
        return {'message': 'Category for windows function should be department or city'}, 404

    if 'type' in data:
        print(data)
        types = data['type'].split('|')
        print(types)
        if len(types) != 2:
            return None, 'When provided, the type query parameter must include both a system and a code.'
        if types[0] not in ['TRE-R66-CategorieEtablissement']:
            return None, "When provided, the system of identifier query parameter must be 'TRE-R66-CategorieEtablissement'"
        data['categorie_etablissement'] = types[1]
        del data['type']
    
    print(data)
    data, _, _ = CareSiteSearchModel.normalize_check_payload(data)

    result = CareSiteSearchModel.group_by(data)
    
    values = [ 
        {
            "name": r[0],
            "count": r[1]
        } 
        for r in result
    ]
    return {
            "resourceType": "windows",
            "method": data['method'],
            "category": data['category'],
            "values": values
            }


@utils.route("/geoloc", methods=["GET"])
@spectree_api.validate(query= GeolocQuery, resp=Response("HTTP_404"), tags=["Utils"])
def get_geoloc():
    data = request.args.to_dict(flat=True)
    if 'type' in data:
        types = data['type'].split('|')
        if len(types) != 2:
            return None, 'When provided, the type query parameter must include both a system and a code.'
        if types[0] not in ['TRE-R66-CategorieEtablissement']:
            return None, "When provided, the system of identifier query parameter must be 'TRE-R66-CategorieEtablissement'"
        data['categorie_etablissement'] = types[1]
        del data['type']
    
    results = CareSiteModel.geo_loc(data)
    return {"count": len(results),
            "results": [ 
                {
                    "finess": r[0],
                    "name": r[1],
                    "x": r[2],
                    "y": r[3]
                } 
                        for r in results]}
