from flask import request, Blueprint
import json
from spectree import Response

from flask import send_file

from server.schema.spec import spectree_api
from server.schema.query.code_system import CodeSystemQuery

code_system = Blueprint("CodeSystem", __name__)

@code_system.route("/CodeSystem", methods=["GET"])
@spectree_api.validate(query=CodeSystemQuery, resp=Response("HTTP_404"), tags=["CodeSystem"])
def get_code_systems():
    data = request.args.to_dict(flat=True)

    if '_id' in data:
        try:
            return send_file("nos/" + data['_id'] + '-FHIR.json')
        except:
            return {"message": "Code System do not exist"}
    if 'url' in data:
        try:
            return send_file("nos/" + data["url"].split('/')[-1] + '-FHIR.json')
        except:
            return {"message": "Code System do not exist"}
        

@code_system.route("/CodeSystem/<string:_id>", methods=["GET"])
@spectree_api.validate(resp=Response("HTTP_404"), tags=["CodeSystem"])
def get_code_system(_id):
        try:
            return send_file("nos/" + _id + '-FHIR.json')
        except:
            return {"message": "Code System do not exist"}



                          
