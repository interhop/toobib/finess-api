from flask import request, Blueprint
from spectree import Response

from server.api.fhir.location import LocationFhir
from server.api.fhir.bundle import BundleFhir
from server.schema.spec import spectree_api
from server.schema.query.location import LocationQuery
from server.cache import cache

location = Blueprint("Location", __name__)

@location.route("/Location", methods=["GET"])
@spectree_api.validate(query=LocationQuery, resp=Response("HTTP_404"), tags=["Location"])
@cache.cached(timeout=604800, query_string=True)
def get_locations():
    query = request.args.to_dict(flat=True)

    if 'address' in query \
            or 'address-postalcode' in query:
        return {'message': 'Only address-city is implemented'}, 404

    if '_id' in query:
        location = LocationFhir.find_by_uuid(query['_id'])
        if location:
            return BundleFhir.get_searchset([location], query).json(), 200
        return {'message': 'Location not found'}, 404

    search, _error_message = LocationFhir.find_from_payload(query)
    if _error_message:
        return {'message': _error_message}, 404

    return BundleFhir.get_searchset(search.locations, query, search).json(), 200

@location.route("/Location/<uuid:_id>", methods=["GET"])
@spectree_api.validate(resp=Response("HTTP_404"), tags=["Location"])
def get_location(_id):
    location = LocationFhir.find_by_uuid(_id)
    if not location:
        return {'message': 'Location not found'}, 404
    return location.json(), 200
