from flask import request, Blueprint
from spectree import Response

from server.schema.spec import spectree_api
from server.schema.query.organization import OrganizationQuery
from server.api.fhir.organization import OrganizationFhir
from server.api.fhir.bundle import BundleFhir


organization = Blueprint("Organization", __name__)

@organization.route("/Organization", methods=["GET"])
@spectree_api.validate(query=OrganizationQuery, resp=Response("HTTP_404"), tags=["Organization"])
def get_organizations():
    # https://werkzeug.palletsprojects.com/en/0.14.x/datastructures/#werkzeug.datastructures.MultiDict.to_dict
    # If flat is True the returned dict will only have the first item present
    query = request.args.to_dict(flat=True)

    if 'address' in query \
            or 'address-postalcode' in query:
        return {'message': 'Only address-city is implemented'}, 404

    if '_id' in query:
        orga = OrganizationFhir.find_by_uuid(query['_id'])
        if orga:
            return BundleFhir.get_searchset([orga], query).json(), 200
        return {'message': 'Organization not found'}, 404

    search, _error_message = OrganizationFhir.find_from_payload(query)
    if _error_message:
        return {'message': _error_message}, 404

    return BundleFhir.get_searchset(search.organizations, query, search).json(), 200


@organization.route("/Organization/<uuid:_id>", methods=["GET"])
@spectree_api.validate(resp=Response("HTTP_404"), tags=["Organization"])
def get_organization(_id):
    orga = OrganizationFhir.find_by_uuid(_id)
    if not orga:
        return {'message': 'Organization not found'}, 404
    return orga.json(), 200
