from pydantic import BaseModel, HttpUrl
from typing import Generic, TypeVar, Optional, List
from pydantic.generics import GenericModel

"""

https://www.hl7.org/fhir/bundle.html

{doco
  "resourceType" : "Bundle",
  // from Resource: id, meta, implicitRules, and language
  "identifier" : { Identifier }, // Persistent identifier for the bundle
  "type" : "<code>", // R!  document | message | transaction | transaction-response | batch | batch-response | history | searchset | collection
  "timestamp" : "<instant>", // When the bundle was assembled
  "total" : "<unsignedInt>", // C? If search, the total number of matches
  "link" : [{ // Links related to this Bundle
    "relation" : "<string>", // R!  See http://www.iana.org/assignments/link-relations/link-relations.xhtml#link-relations-1
    "url" : "<uri>" // R!  Reference details for the link
  }],
  "entry" : [{ // Entry in the bundle - will have a resource or information
    "link" : [{ Content as for Bundle.link }], // Links related to this entry
    "fullUrl" : "<uri>", // URI for resource (Absolute URL server address or URI for UUID/OID)
    "resource" : { Resource }, // A resource in the bundle
    "search" : { // C? Search related information
      "mode" : "<code>", // match | include | outcome - why this is in the result set
      "score" : <decimal> // Search ranking (between 0 and 1)
    },
    "request" : { // C? Additional execution information (transaction/batch/history)
      "method" : "<code>", // R!  GET | HEAD | POST | PUT | DELETE | PATCH
      "url" : "<uri>", // R!  URL for HTTP equivalent of this entry
      "ifNoneMatch" : "<string>", // For managing cache currency
      "ifModifiedSince" : "<instant>", // For managing cache currency
      "ifMatch" : "<string>", // For managing update contention
      "ifNoneExist" : "<string>" // For conditional creates
    },
    "response" : { // C? Results of execution (transaction/batch/history)
      "status" : "<string>", // R!  Status response code (text optional)
      "location" : "<uri>", // The location (if the operation returns a location)
      "etag" : "<string>", // The Etag for the resource (if relevant)
      "lastModified" : "<instant>", // Server's date time modified
      "outcome" : { Resource } // OperationOutcome with hints and warnings (for batch/transaction)
    }
  }],
  "signature" : { Signature } // Digital Signature
}

"""

DataT = TypeVar('DataT')

class LinkSchema(BaseModel):
    relation: str
    url: HttpUrl

class EntrySchema(GenericModel, Generic[DataT]):
    fullURL: HttpUrl
    resource: DataT

class BundleSchema(GenericModel, Generic[DataT]):
    resourceType: str = "Bundle"
    total: int
    link: List[LinkSchema]
    type: str = "searchset"
    entry: List[EntrySchema[DataT]]
