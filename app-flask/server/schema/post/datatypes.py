# DataTypes FHIR documentation : https://www.hl7.org/fhir/datatypes.html

from typing import List, Optional
from enum import Enum
from pydantic import BaseModel, Field, PositiveInt, constr, conlist, root_validator, Extra
from datetime import datetime

class IdentifierUseEnum(str, Enum):
    # usual = 'usual'
    official = 'official'
    #temp = 'temp'
    #secondary = 'secondary'
    #old = 'old'

class Coding(BaseModel):

    """
    https://www.hl7.org/fhir/datatypes.html#Coding
    
    {doco
      // from Element: extension
      "system" : "<uri>", // Identity of the terminology system
      "version" : "<string>", // Version of the system - if relevant
      "code" : "<code>", // Symbol in syntax defined by the system
      "display" : "<string>", // Representation defined by the system
      "userSelected" : <boolean> // If this coding was chosen directly by the user
    } 
    """
    system: str
    code: str
    display: str
    ## Hidden
    _code_in: List[str] = []

    class Config:
        underscore_attrs_are_private = True
        extra = Extra.ignore

    #@root_validator()
    #def code_in_code_in(cls, values):
    #    code = values.get("code")
    #    code_in = values.get("code_in")
    #    if len(code_in) and code not in code_in:
    #        raise ValueError(
    #            f"{code} is not in {code_in}"
    #        )
    #    return values

class CodeableConcept(BaseModel):
    """
    https://www.hl7.org/fhir/datatypes.html#CodeableConcept

    {doco
      // from Element: extension
      "coding" : [{ Coding }], // Code defined by a terminology system
      "text" : "<string>" // Plain text representation of the concept
    }


    	"valueCodeableConcept": {
		"coding": [
			{
				"system": "http://snomed.info/sct",
				"code": "260385009",
				"display": "Negative"
			}, {
				"system": "https://acme.lab/resultcodes",
				"code": "NEG",
				"display": "Negative"
			}
		],
		"text": "Negative for Chlamydia Trachomatis rRNA"
	}

    """
    coding: conlist(item_type=Coding, min_items=1, max_items=1)
    text: str

    class Config:
        extra = Extra.ignore
 
class Identifier(BaseModel):
    """
    https://www.hl7.org/fhir/datatypes.html#Identifier
    
    {doco
      "use" : "<code>", // usual | official | temp | secondary | old (If known)
      "type" : { CodeableConcept }, // Description of identifier
      "system" : "<uri>", // The namespace for the identifier value
      "value" : "<string>", // The value that is unique
      "period" : { Period }, // Time period when id is/was valid for use
      "assigner" : { Reference(Organization) } // Organization that issued id (may be just text)
    }
    """
    use: IdentifierUseEnum = IdentifierUseEnum.official
    type: CodeableConcept
    system: constr(min_length=1)
    value: constr(min_length=1)

    class Config:
        extra = Extra.ignore


class Extension(BaseModel):

    """
    https://www.hl7.org/fhir/extensibility.html 
    example : https://www.hl7.org/fhir/basic-example.json.html

    {doco
      // from Element: extension
      "url" : "<uri>", // R!  identifies the meaning of the extension
      // value[x]: Value of extension. One of these 50:
      "valueBase64Binary" : "<base64Binary>"
      "valueBoolean" : <boolean>
      "valueCanonical" : "<canonical>"
      "valueCode" : "<code>"
      "valueDate" : "<date>"
      "valueDateTime" : "<dateTime>"
      "valueDecimal" : <decimal>
      "valueId" : "<id>"
      "valueInstant" : "<instant>"
      "valueInteger" : <integer>
      "valueMarkdown" : "<markdown>"
      "valueOid" : "<oid>"
      "valuePositiveInt" : "<positiveInt>"
      "valueString" : "<string>"
      "valueTime" : "<time>"
      "valueUnsignedInt" : "<unsignedInt>"
      "valueUri" : "<uri>"
      "valueUrl" : "<url>"
      "valueUuid" : "<uuid>"
      "valueAddress" : { Address }
      "valueAge" : { Age }
      "valueAnnotation" : { Annotation }
      "valueAttachment" : { Attachment }
      "valueCodeableConcept" : { CodeableConcept }
      "valueCoding" : { Coding }
      "valueContactPoint" : { ContactPoint }
      "valueCount" : { Count }
      "valueDistance" : { Distance }
      "valueDuration" : { Duration }
      "valueHumanName" : { HumanName }
      "valueIdentifier" : { Identifier }
      "valueMoney" : { Money }
      "valuePeriod" : { Period }
      "valueQuantity" : { Quantity }
      "valueRange" : { Range }
      "valueRatio" : { Ratio }
      "valueReference" : { Reference }
      "valueSampledData" : { SampledData }
      "valueSignature" : { Signature }
      "valueTiming" : { Timing }
      "valueContactDetail" : { ContactDetail }
      "valueContributor" : { Contributor }
      "valueDataRequirement" : { DataRequirement }
      "valueExpression" : { Expression }
      "valueParameterDefinition" : { ParameterDefinition }
      "valueRelatedArtifact" : { RelatedArtifact }
      "valueTriggerDefinition" : { TriggerDefinition }
      "valueUsageContext" : { UsageContext }
      "valueDosage" : { Dosage }
      "valueMeta" : { Meta }
    }

    """

    url: str

class ExtensionCodeableConcept(Extension):
    url: CodeableConcept

    class Config:
        extra = Extra.ignore


class HumanUseEnum(str, Enum):
    # usual = 'usual'
    official = 'official'
    #temp = 'temp'
    #secondary = 'secondary'
    #old = 'old'

class HumanName(BaseModel):
    """
    https://www.hl7.org/fhir/datatypes.html#HumanName
    
    {
      "use" : "<code>", // usual | official | temp | nickname | anonymous | old | maiden
      "text" : "<string>", // Text representation of the full name
      "family" : "<string>", // Family name (often called 'Surname')
      "given" : ["<string>"], // Given names (not always 'first'). Includes middle names
      "prefix" : ["<string>"], // Parts that come before the name
      "suffix" : ["<string>"], // Parts that come after the name
      "period" : { Period } // Time period when name was/is in use
    }
    """

    use: HumanUseEnum = HumanUseEnum.official
    text: str
    family : str
    given : conlist(str, max_items=1)
    prefix : conlist(str, max_items=1)
    suffix : conlist(str, max_items=1)

    class Config:
        extra = Extra.ignore

class AddressUseEnum(str, Enum):
    home = 'home'
    work = 'work'
    temp = 'temp'
    old = 'old'
    billing = 'billing'
 
class AddressTypeEnum(str, Enum):
    postal = 'postal'
    physical = 'physical'
    both = 'both'
 
class Address(BaseModel):
    """
    https://www.hl7.org/fhir/datatypes.html#Address
    
    {
      "use" : "<code>", // home | work | temp | old | billing - purpose of this address
      "type" : "<code>", // postal | physical | both
      "text" : "<string>", // Text representation of the address
      "line" : ["<string>"], // Street name, number, direction & P.O. Box etc.
      "city" : "<string>", // Name of city, town etc.
      "district" : "<string>", // District name (aka county)
      "state" : "<string>", // Sub-unit of country (abbreviations ok)
      "postalCode" : "<string>", // Postal code for area
      "country" : "<string>", // Country (e.g. can be ISO 3166 2 or 3 letter code)
      "period" : { Period } // Time period when address was/is in use
    }
    """
    use: AddressUseEnum = AddressUseEnum.work
    type: AddressTypeEnum = AddressTypeEnum.postal
    text: str
    line : List[str]
    city : Optional[str]
    postal_code : Optional[str]
    district : Optional[str]
    state : Optional[str]
    country : Optional[str]
    extension : Extension

    class Config:
        extra = Extra.ignore

class ContactSystemEnum(str, Enum):
    phone = 'phone'
    fax = 'fax'
    email = 'email'
    pager = 'pager'
    url = 'url'
    sms = 'sms'
    other = 'other'
 
class ContactUseEnum(str, Enum):
    home = 'home'
    work = 'work'
    temp = 'temp'
    old = 'old'
    mobile = 'mobile'

class ContactPoint(BaseModel):
    """
    https://www.hl7.org/fhir/datatypes.html#ContactPoint
    
    {doco
      "system" : "<code>", // C? phone | fax | email | pager | url | sms | other
      "value" : "<string>", // The actual contact point details
      "use" : "<code>", // home | work | temp | old | mobile - purpose of this contact point
      "rank" : "<positiveInt>", // Specify preferred order of use (1 = highest)
      "period" : { Period } // Time period when the contact point was/is in use
    }
    """
    system: ContactSystemEnum
    value: str
    use: ContactUseEnum = ContactUseEnum.work
    rank : PositiveInt

    class Config:
        extra = Extra.ignore

class Periode(BaseModel):
    """
    https://www.hl7.org/fhir/datatypes.html#Period
    
    {
      "start" : "<dateTime>", // C? Starting time with inclusive boundary
      "end" : "<dateTime>" // C? End time with inclusive boundary, if not ongoing
    }
    """
    start: datetime
    end: datetime

    class Config:
        extra = Extra.ignore
