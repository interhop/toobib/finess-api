from uuid import UUID
from typing import List
from pydantic import BaseModel, Extra
from .datatypes import HumanName, Identifier, ContactPoint, Address, CodeableConcept, Extension

"""

https://www.hl7.org/fhir/organization.html

{doco
  "resourceType" : "Organization",
  // from Resource: id, meta, implicitRules, and language
  // from DomainResource: text, contained, extension, and modifierExtension
  "identifier" : [{ Identifier }], // C? Identifies this organization  across multiple systems
  "active" : <boolean>, // Whether the organization's record is still in active use
  "type" : [{ CodeableConcept }], // Kind of organization
  "name" : "<string>", // C? Name used for the organization
  "alias" : ["<string>"], // A list of alternate names that the organization is known as, or was known as in the past
  "telecom" : [{ ContactPoint }], // C? A contact detail for the organization
  "address" : [{ Address }], // C? An address for the organization
  "partOf" : { Reference(Organization) }, // The organization of which this organization forms a part
  "contact" : [{ // Contact for the organization for a certain purpose
    "purpose" : { CodeableConcept }, // The type of contact
    "name" : { HumanName }, // A name associated with the contact
    "telecom" : [{ ContactPoint }], // Contact details (telephone, email, etc.)  for a contact
    "address" : { Address } // Visiting or postal addresses for the contact
  }],
  "endpoint" : [{ Reference(Endpoint) }] // Technical endpoints providing access to services operated for the organization
}
"""

class Position(BaseModel):
    longitude: float
    latitude: float
    altitude: float

    class Config:
        extra = Extra.ignore

class Location(BaseModel):
    id: UUID
    resourceType: str = "Location"
    identifier: List[Identifier]
    status: str = "active"
    type: List[CodeableConcept]
    name: str
    alias: List[str]
    telecom: List[ContactPoint] 
    address: List[FrenchAddress]
    address: Position

    class Config:
        extra = Extra.ignore
