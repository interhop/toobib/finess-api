from typing import List, Optional
from enum import Enum
from pydantic import BaseModel, Field
from datetime import datetime

class OrganizationQuery(BaseModel):
    id: Optional[str] = Field(alias="_id", default="8bc8db4a-654d-4408-b618-1ddcc28fd514", description="The resource uuid associated with the resource.")
    identifier: Optional[str] = Field(default="finess|010000024", description="Enter finess or siret or siret")
    type : Optional[str] = Field(default="TRE-R66-CategorieEtablissement|620", description="Type of organization, system should be 'TRE-R66-CategorieEtablissement'")
    #address : Optional[str] = Field()
    # address_postalcode : Optional[List[str]] = Field(alias="address-postalcode")
    address_city : Optional[str] = Field(alias="address-city", description="A city specified in an address.")
    french_department : Optional[str] = Field(alias="french-department", default="TRE-G09-DepartementOM|75", description="A french department, system should be 'TRE-G09-DepartementOM'")
    name : Optional[str] = Field(description="Name used for the organization")
    text : Optional[str] = Field(alias="_text", description="Free text to search on the narrative of the resource (given and name).")
    count : Optional[int] = Field(alias="_count", description="The maximum number of results to return. Defaults to 12. Should be a posive number <= 1000")
    page : Optional[int] = Field(alias="page", description="Internal Toobib Server parameters (not FHIR parameters). Pagination. Defaults to 1.")
