from typing import List, Optional
from enum import Enum
from pydantic import BaseModel, Field
from datetime import datetime
from enum import Enum

class Method(str, Enum):
    count = 'count'

class Category(str, Enum):
    department = 'department'
    city = 'city'

class WindowQuery(BaseModel):
    method : Method = Field(description="Window function method, only count (group by) is allowed")
    category : Category = Field(description="The category to group by. Should be 'department' or 'city'")
    type : Optional[str] = Field(default="TRE-R66-CategorieEtablissement|620", description="Type of organization, system should be 'TRE-R66-CategorieEtablissement'")
    #address_city : Optional[str] = Field(alias="address-city", description="A city specified in an address.")
    name : Optional[str] = Field(description="Name used for the organization")
    #text : Optional[str] = Field(alias="_text", description="Free text to search on the narrative of the resource (given and name).")

class GeolocQuery(BaseModel):
    type : Optional[str] = Field(default="type=TRE-R66-CategorieEtablissement|620", description="Type of organization, system should be 'TRE-R66-CategorieEtablissement'")

