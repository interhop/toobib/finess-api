from typing import List, Optional
from enum import Enum
from pydantic import BaseModel, Field

class CodeSystemQuery(BaseModel):
    id: Optional[str] = Field(alias="_id", default="TRE-G09-DepartementOM", description="The resource id associated with the resource.")
    identifier: Optional[str] = Field(default="https://mos.esante.gouv.fr/NOS/TRE-G09-DepartementOM/FHIR/TRE-G09-DepartementOM", description="The uri that identifies the code system")
