import os

from flask import Flask
from flask_cors import CORS

from server.cache import cache
from server.config import TestingConfig

from server.api.resources.organization import organization
from server.api.resources.location import location
from server.api.resources.code_system import code_system
from server.api.resources.utils import utils
from server.schema.spec import spectree_api

from server.blocklist import BLOCKLIST

# Create app and API
def create_app(test_config=None):
    app = Flask(__name__, instance_relative_config=True)
    #cache.init_app(app, config={'CACHE_TYPE': 'simple'})
    cache.init_app(app, config={'CACHE_TYPE': 'redis', 'CACHE_REDIS_URL': 'redis://localhost:6379/0'})
    
    if test_config is None:
            # load the instance config, if it exists, when not testing
            app.config.from_object(TestingConfig())
    else:
            # load the test config if passed in
            app.config.from_mapping(test_config)

    # ensure the instance folder exists
    try:
            os.makedirs(app.instance_path)
    except OSError:
            pass

    from .db import db
    import logging
    
    logging.basicConfig(filename='error.log', level=logging.DEBUG)
    
    db.init_app(app)

    # Open cross url
    cors = CORS(app, resources={r"/foo": {"origins": "*"}})
    app.config['CORS_HEADERS'] = 'Content-Type'

    app.register_blueprint(organization)
    app.register_blueprint(location)
    app.register_blueprint(code_system)
    app.register_blueprint(utils)
    spectree_api.register(app)
   
    return app
