from unidecode import unidecode 
import re
my_dict = {
	"libelleCommune" : "libelle_commune",
	"libelleProfession" : "libelle_profession",
}

def normalizeStrings(dico):
    """
        erase '-', lower()
        unidecode : unidecode('ko\u017eu\u0161\u010dek') -> 'kozuscek'
        del empty key for dict
    """
    new_dico = []
    for k in dico:
        k = unidecode(re.sub(r"[-]","", k)).lower()
        new_dico.append(k)
    return new_dico
