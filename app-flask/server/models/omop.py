from ..config import Config
from ..db import db
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.orm import load_only
from flask.json import JSONEncoder
from sqlalchemy import func
from flask import jsonify

## ##############################
## Standardized Vocabularies
## ##############################

class ConceptModel(db.Model):
    __tablename__ = 'concept'
    __table_args__ = {'schema': Config.OMOP_SCHEMA
            #, 'extend_existing': True
            }

    id = db.Column('concept_id', db.Integer, primary_key=True,autoincrement=True)
    concept_name= db.Column(db.Text())
    domain_id= db.Column(db.Text())
    vocabulary_id= db.Column(db.Text())
    standard_concept= db.Column(db.Text())
    concept_code= db.Column(db.Text())
    valid_start_date= db.Column(db.Text())
    valid_end_date= db.Column(db.Text())
    invalid_reason= db.Column(db.String(1))

    # ##############
    # FINESS MODEL
    # #############

    # CareSiteModel
    departements = db.relationship('CareSiteModel', backref='departement', lazy='dynamic', foreign_keys='CareSiteModel.departement_concept_id')
    categorie_etablissements = db.relationship('CareSiteModel', backref='categorie_etablissement', lazy='dynamic', foreign_keys='CareSiteModel.categorie_etablissement_concept_id')
    categorie_agregat_etablissemens = db.relationship('CareSiteModel', backref='categorie_agregat_etablissement', lazy='dynamic', foreign_keys='CareSiteModel.categorie_agregat_etablissement_concept_id')
    sphs = db.relationship('CareSiteModel', backref='sph', lazy='dynamic', foreign_keys='CareSiteModel.sph_concept_id')
    mfts = db.relationship('CareSiteModel', backref='mft', lazy='dynamic', foreign_keys='CareSiteModel.mft_concept_id')

    @classmethod
    def find_by_vocabulary(cls, vocabulary_id, concept_code):
        return cls.query.filter_by(vocabulary_id=vocabulary_id, concept_code=concept_code).first()
