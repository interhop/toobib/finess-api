from ..config import Config
from ..util import normalizeStrings
from ..db import db
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.orm import load_only
from sqlalchemy import or_, func
from unidecode import unidecode 
import re
from ..util import normalizeStrings
## ##############################
## Materialized Views
## ##############################

class CareSiteSearchModel(db.Model):
    __tablename__ = 'care_site_search'
    __table_args__ = ({'schema': Config.FINESS_SCHEMA})

    care_site_id = db.Column(db.Text(), primary_key=True)
    care_site_uuid = db.Column(UUID(as_uuid=True))
    finess_et = db.Column(db.Text())
    finess_ej = db.Column(db.Text())
    siret = db.Column(db.Text())
    nom = db.Column(db.Text())
    nom_normalise = db.Column(db.Text())
    departement = db.Column(db.Text())
    categorie_etablissement = db.Column(db.Text())
    code_postal = db.Column(db.Text())
    commune = db.Column(db.Text())
    commune_normalise = db.Column(db.Text())

    @classmethod
    def find_by_care_site(cls, care_site_id):
        return cls.query.filter_by(care_site_id = care_site_id).first()

    @classmethod
    def normalize_check_payload(cls, dico):
        """
            data_base <-> FHIR terms

            if str :
                - erase '-', lower()
                - unidecode : unidecode('ko\u017eu\u0161\u010dek') -> 'kozuscek'
            del empty key for dict

            return new_dico, page, limit

            pop = https://kite.com/python/answers/how-to-rename-a-dictionary-key-in-python
            change javascript standard variable declaration to PEP
        """
        normalize_dico = {
            "address-city" : "commune_normalise",
            "name" : "nom_normalise",
        }

        new_dico = {}
        page = 1
        limit = 12

        # Erase all keys that should not be processed by bundle_search function
        if 'page' in dico:
            page = int(dico['page'])
            del dico["page"]
        if '_count' in dico:
            limit = int(dico['_count'])
            del dico["_count"]

        # Fhir to database traduction
        for k in dico:
            if type(dico[k]) is str:
                dico[k] = unidecode(re.sub(r"[-]","", dico[k])).lower()
            if dico[k]: # if strings[key] is None -> del
                if k in normalize_dico:
                    new_dico[normalize_dico[k]] = dico[k]
                else:
                    new_dico[k] = dico[k]

       
        return new_dico, page, limit
 
    @classmethod
    def search(cls, data, page, limit):
        """
            Find organization in  table
            Use GIN index of postgreSQL, cf READ.md
            If search parameter is filled others are ignored

            Use Paginate function : https://flask-sqlalchemy.palletsprojects.com/en/2.x/api/#flask_sqlalchemy.Pagination
        """
        query_set = cls.query

        if '_text' in data:
            words = data['_text'].split(' ')
            del data['_text']
            for w in words:
                query_set = query_set.filter(or_(
                    cls.nom_normalise.like('%' + w + '%'),
                    cls.commune_normalise.like('%' + w + '%')
            ))
        if 'name' in data:
            words = data['name'].split(' ')
            del data['name']
            for w in words:
                query_set = query_set.filter(or_(
                    cls.nom_normalise.like('%' + w + '%'),
            ))


        # commune_normalise, nom_normalise, departement_normalise
        for k, v in data.items():
            if v and k in ['commune_normalise', 'nom_normalise', 'departement']:
                query_set = query_set.filter(getattr(cls, k).like('%' + v + '%'))
        for k, v in data.items():
            if v and k in ['finess_et', 'finess_ej', 'siret', 'categorie_etablissement']:
                print(k, v)
                query_set = query_set.filter(getattr(cls, k) ==  v )

        # pagination
        return query_set.order_by(cls.nom_normalise).paginate(page=page,
                per_page=limit, error_out=True, max_per_page=limit)
    
    @classmethod
    def group_by(cls, data):
        data['category'] = 'departement' if data['category'] == 'department' else data['category']
        data['category'] = 'commune' if data['category'] == 'city' else data['category']

        query_set = db.session.query(getattr(cls, data['category']), func.count(getattr(cls, data['category'])))

        if '_text' in data:
            words = data['_text'].split(' ')
            del data['_text']
            for w in words:
                query_set = query_set.filter(or_(
                    cls.nom_normalise.like('%' + w + '%'),
                    cls.commune_normalise.like('%' + w + '%')
            ))
        if 'name' in data:
            words = data['name'].split(' ')
            del data['name']
            for w in words:
                query_set = query_set.filter(or_(
                    cls.nom_normalise.like('%' + w + '%'),
            ))

        # commune_normalise, nom_normalise, departement_normalise
        for k, v in data.items():
            if v and k in [ 'nom_normalise', 'commune_normalise']:
                query_set = query_set.filter(getattr(cls, k).like('%' + v + '%'))
        for k, v in data.items():
            if v and k in ['finess_et', 'finess_ej', 'siret', 'categorie_etablissement']:
                print(k, v)
                query_set = query_set.filter(getattr(cls, k) ==  v )

        return query_set.group_by(getattr(cls, data['category'])).order_by(getattr(cls, data['category'])).all()
