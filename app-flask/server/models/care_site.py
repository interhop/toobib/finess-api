from ..config import Config
from ..db import db
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.orm import load_only
from flask.json import JSONEncoder
from sqlalchemy import func
from flask import jsonify
from .omop import ConceptModel


class CareSiteModel(db.Model):
    __tablename__ = 'care_site'
    __table_args__ = {'schema': Config.FINESS_SCHEMA
            #, 'extend_existing': True
            }

    id = db.Column('care_site_id', db.Integer, primary_key=True,autoincrement=True)
    uuid = db.Column('care_site_uuid', UUID(as_uuid=True))
    finess_et = db.Column(db.Text())
    finess_ej  = db.Column(db.Text())
    raison_sociale = db.Column(db.Text())
    raison_sociale_longue = db.Column(db.Text())
    complement_raison_sociale = db.Column(db.Text())
    complement_distribution = db.Column(db.Text())
    numero_voie = db.Column(db.Text())
    type_voie = db.Column(db.Text())
    libelle_voie = db.Column(db.Text())
    complement_voie = db.Column(db.Text())
    lieu_dit_bp = db.Column(db.Text())
    code_commune = db.Column(db.Text())
    departement_concept_id = db.Column(db.Integer, db.ForeignKey('{}.concept.concept_id'.format(Config.OMOP_SCHEMA)))
    ligne_acheminement = db.Column(db.Text())
    code_postal = db.Column(db.Text())
    commune = db.Column(db.Text())
    telephone = db.Column(db.Text())
    telecopie = db.Column(db.Text())
    categorie_etablissement_concept_id = db.Column(db.Integer, db.ForeignKey('{}.concept.concept_id'.format(Config.OMOP_SCHEMA)))
    categorie_agregat_etablissement_concept_id = db.Column(db.Integer, db.ForeignKey('{}.concept.concept_id'.format(Config.OMOP_SCHEMA)))
    siret = db.Column(db.Text())
    code_ape = db.Column(db.Text())
    mft_concept_id = db.Column(db.Integer, db.ForeignKey('{}.concept.concept_id'.format(Config.OMOP_SCHEMA)))
    sph_concept_id = db.Column(db.Integer, db.ForeignKey('{}.concept.concept_id'.format(Config.OMOP_SCHEMA)))
    date_ouverture = db.Column(db.Text())
    date_autorisation = db.Column(db.Text())
    date_maj_structure = db.Column(db.Text())
    numero_education_national = db.Column(db.Text())
              
    lambert_coord_x = db.Column(db.Float)			   
    lambert_coord_y = db.Column(db.Float)                        
    wsg_coord_x = db.Column(db.Float)				   
    wsg_coord_y = db.Column(db.Float)				   
    code_commune_coord_x = db.Column(db.Float)                    
    code_commune_coord_y = db.Column(db.Float)                    
    code_postal_coord_x = db.Column(db.Float)                    
    code_postal_coord_y = db.Column(db.Float)                    

    insert_datetime = db.Column(db.DateTime())

    @classmethod
    def find_by_id(cls, _id):
        return  cls.query.filter_by(id = _id).first() # no doublon

    @classmethod
    def find_by_uuid(cls, uuid):
        return cls.query.filter_by(uuid = uuid).first()

    @classmethod
    def find_by_finess_et(cls, finess_et):
        return cls.query.filter_by(finess_et = finess_et).first()

    @classmethod
    def find_by_finess_ej(cls, finess_ej):
        return cls.query.filter_by(finess_ej = finess_ej).first()




    @classmethod
    def geo_loc(cls,  data):

        query_set = db.session.query(cls.finess_et, cls.raison_sociale, cls.wsg_coord_x, cls.wsg_coord_y)

        for k, v in data.items():
            if v and k in [ 'categorie_etablissement']:
                query_set = query_set.filter(cls.categorie_etablissement_concept_id == ConceptModel.find_by_vocabulary("TRE-R66-CategorieEtablissement", v ).id)

        return query_set.all()
